<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route :: get('test',function()
{
      return "test project api";
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/api/municipal/Ahmedabad/deptadmindash',function(){
	return view('deptadmindash');
});
Route::get('/api/municipal/Ahmedabad/addauthority',function(){

	return view('addauthority');
});
Route::get('/api/municipal/Ahmedabad/AverageAnalysis',function(){

	return view('AverageAnalysis');
});
Route::get('/deptlogin',function(){
	return view('deptlogin');

});
//view authority
Route::get('/viewauthority',function(){
	return view('viewauthority');
});

Route::get('/demo',function(){
	return view('demo');
});
// file for inserting analysis page
Route::get('/testdemo',function(){
	return view('templatepage');
});
//authority login
Route::get('/authlogin',function(){
	return view('authlogin');
});
//authority dashboard
Route::get('/authdash',function(){
	return view('authdash');
});
//user complaints

Route::get('/viewcomplaint',function(){
	return view('viewcomplaint');
});
Route::get('/wardlogin',function(){
	return view('wardlogin');
});
Route::get('/warddash',function(){
	return view('warddash');
});
//test routes
Route::get('/addadmin',function(){
	return view('addadmin');
});
Route::get('/viewdept',function(){
	return view('viewdeptadmin');
});
Route::get('/deptdash',function(){
	return view('deptadmindash');
});
Route::get('/deptanalysis',function(){
	return view('/DepartmentAnalysis');
});
Route::get('/areaanalysis',function(){
	return view('/AreaAnalysis');
});
Route::get('/complains',function(){
	return view('complains');
});


Route::get('cert_template',function(){
	return view('cert_template');
});
Route::get('/super',function(){
	return view('superadminlogin');
});




Route::get('test/test','DepartmentAdminController@test');