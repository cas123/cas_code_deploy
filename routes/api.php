<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Auth :: routes(['verify' => true]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route :: get('test',function()
{
      return "test project api";
});
// Route :: post('migrationtest','RegisterController@register');
// Route :: get('/',function()
// {

// 	return view('welcome');
// });


//defining named routed for views
//@all the view routes in this section
Route::get('addauthority',function()
{
      return view('addauthority');
})->name('addauthority');
Route::get('viewauthority1',function()
{
      return view('viewauthority1');
})->name('viewauthority1');
Route::get('average_analysis',function()
{
      return view('AverageAnalysis');
})->name('average_analysis');
Route::get('employee_analysis',function()
{
      return view('EmployeeAnalysis');
})->name('employee_analysis');



Route::get('deptadmindash',function()
{
      return view('deptadmindash');
})->name('deptadmindash');



Route::get('authdash',function()
{
      return view('authdash');
})->name('authdash');

Route::get('warddash',function()
{
      return view('warddash');
})->name('warddash');
Route::get('addadmin',function()
{
      return view('addadmin');
})->name('addadmin');
Route::get('viewdeptadmin',function()
{
      return view('viewdeptadmin');
})->name('viewdeptadmin');

Route::get('DepartmentAnalysis',function()
{
      return view('DepartmentAnalysis');
})->name('DepartmentAnalysis');
Route::get('AreaAnalysis',function()
{
      return view('AreaAnalysis');
})->name('AreaAnalysis');

Route::get('complains',function(){
  return view('complains');
})->name('complains');
Route::get('solvedproof',function(){
  return view('solvedproof');
})->name('solvedproof');
Route::get('superadmindash',function(){
  return view('superadmindash');
})->name('superadmindash');
Route::get('addwardadmin',function(){
  return view('addwardadmin');
})->name('addwardadmin');
Route::get('viewwardadmin',function(){
  return view('viewwardadmin');
})->name('viewwardadmin');
Route::get('wardwise',function(){
  return view('wardwise');
})->name('wardwise');
Route::get('longpendingcomplains',function(){
  return view('longpendingcomplains');
})->name('longpendingcomplains');
Route::get('deptpendingcomplains',function(){
  return view('deptpendingcomplains');
})->name('deptpendingcomplains');



Route::get('wardpendingcomplains',function(){
  return view('wardpendingcomplains');
})->name('wardpendingcomplains');


Route::post('testchange','WardAdminController@test');
//Route::view('','addauthority')->name('addauthority');

//
Route::post('forgotpassword','AuthorityController@sendPassword');

Route::get('generatecertificate','UsersController@generateCertificate');

Route::prefix('municipal')->group(function () {





    //authority routes
  //authority routes



  Route::prefix('authority')->group(function(){


          Route::post('login','AuthorityController@logIn')->middleware('auth_login');
          Route::post('getcomplains','AuthorityController@fetchcomplains');
          Route::get('usercomplaints',function()
         {
            return view('complains');
                })->name('complains');
          Route::post('sendnotification','AuthorityController@sendnotification');

          Route::post('solvedrequest','AuthorityController@getSolutionProof');

        });



      Route:: prefix('admin')->group(function()
    {
                     Route::get('login','SuperAdminController@adminLogin')->middleware('login');
                     Route::delete('logout','SuperAdminController@adminLogout');

                         Route:: prefix('analysis')->group(function()
                   {
                      Route::get('wardwise','SuperAdminController@getWardWiseAnalysis');
                      Route::get('supertrend','SuperAdminController@superTrend');
                      Route::post('topwards','SuperAdminController@getTopWards');
                      Route::post('topdepartments','SuperAdminController@getTopDepartments');
                      Route::post('topareas','SuperAdminController@getTopAreas');
                      Route::post('getoverall','SuperAdminController@getOverall');
                    
                     Route::post('overalltrend','SuperAdminController@getOverAllTrend');

                     
                  });


   });
    Route::post('register','SuperAdminController@register');
    Route::get('details','SuperAdminController@getMunicipalTable');
    Route::get('forgotpas','SuperAdminController@forgotPassword');
    Route::get('login','SuperAdminController@adminLogin');
    Route::get('verification/{email_id}','SuperAdminController@emailVerified');
    Route::post('addwardadmin','SuperAdminController@addWardAdmin');
    Route::get('getwards','SuperAdminController@getAllWards');
    Route::get('getallemployees','SuperAdminController@getAllEmployees');
    Route::get('tes','SuperAdminController@getAllEmployees');
    Route::get('addedwardadmin','SuperAdminController@getAddedWardAdmin');
    Route::get('deletewardadmin','SuperAdminController@removeWardAdmin');
    Route::post('forgotpassword','SuperAdminController@sendPassword');



    
    
     
   //users of particular municipals
     Route::prefix('users')->group(function(){

      Route::post('register','UsersController@register');
      Route::post('login','UsersController@login')->middleware('userslogin');
      Route::delete('logout','UsersController@logout');  
      Route::post('complains','UsersController@registerComplain');
      Route::post('track','UsersController@getStatus'); 
      Route::post('update','UsersController@updateProfile');
      Route::post('forgotpassword','UsersController@forgotPassword');
      Route::post('changepassword','UsersController@changePassword');
      Route::post('checksatisfaction','UsersController@checkSatisfaction');
       Route::post('getallcomplains','UsersController@getAllComplains');
       Route::post('getnotifications','UsersController@giveAllRecentNotifications');
       Route::post('getcertificates','UsersController@giveAllCertificates');

 


   });

     Route:: prefix('Ahmedabad')->group(function()
     {
 
        

          Route::prefix('wardanalysis')->group(function(){


                         Route::post('departmentwise','WardAdminController@getDeptwiseAnalysis');
                         Route::post('areawise','WardAdminController@getAreaWiseAnalysis');
                          Route::post('deptmonthtrend','WardAdminController@getDeptMonthTrendAnalysis');
                          Route::post('topdepartments','WardAdminController@getTopDepartments');
                           Route::post('topareas','WardAdminController@getTopAreas');

            });
       

          Route::post('addeddepartmentadmin','WardAdminController@getAddedDepartmentAdmin');
          Route::get('deletedepartmentadmin','WardAdminController@removeDepartmentAdmin');

        
        Route::get('wardAdminRegister','WardAdminController@sendLinkForLogin');
        Route::get('wardlogin','WardAdminController@LogIn')->middleware('wardadminlogin');
        Route::delete('wardlogout','WardAdminController@LogOut');
        Route::post('adddepartmentadmin','WardAdminController@addDepartmentAdmin');
         Route::get('getdepartments','WardAdminController@getAllDepartments');
         Route::get('departmentemployees','WardAdminController@getAllEmployees');
        Route::get('departmentlogin','DepartmentAdminController@LogIn')->middleware('departmentadminlogin');
        Route::delete('departmentlogout','DepartmentAdminController@LogOut');



     });
     Route::prefix('department')->group(function()
     {
             Route::prefix('employees')->group(function()
               {
                      Route::get('fetch','DepartmentAdminController@getEmployees');
                      Route::any('addAuthorities','DepartmentAdminController@addAuthority');
                       Route::get('removeAuthorities','DepartmentAdminController@removeAuthority');
                      Route::post('addedauthorities','DepartmentAdminController@getAddedAuthorities');



               });
   


               Route::prefix('analytics')->group(function()
               {
                     
                      Route::POST('/','DepartmentAdminController@getAnalytics');
                      Route::post('topareas','DepartmentAdminController@getTopAreas');

               });
     });


});


