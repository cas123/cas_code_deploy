<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredMunicipals extends Model
{
    //
    protected $table = 'registered_municipals';
    protected $fillable = ['municipal_id','employee_id','activation_token','municipal_name','database_name','phone','is_verified','is_admin','last_online','used_storage'];
    protected $guarded = ['id','created_at','updated_at'];


}
