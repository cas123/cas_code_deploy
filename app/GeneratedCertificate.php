<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedCertificate extends Model
{
    //
    protected $connection='ahmedabad_connection';
    protected $table='generated_certificate';
    protected $fillable = ['user_id','certificate_link','complain_id'];
    protected $guarded=['id','created_at','updated_at'];
}
