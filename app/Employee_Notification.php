<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_Notification extends Model
{
    //
      protected $connection = 'ahmedabad_connection';
      protected $table = 'employee_notification';
      protected $fillable = ['from_user_id','to_employee_id','content','type','is_read'];
      protected $guarded = ['id','created_at','updated_at'];

}
