<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailForAddView extends Mailable
{
    use Queueable, SerializesModels;
    public $data1;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
           array_push($this->data1,$data1);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        public $data;
        $data=$this->data1;

        return $this->view('AddTemplate')->with('data',$data);
    }
}
