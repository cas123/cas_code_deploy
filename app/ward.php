<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ward extends Model
{
    //
     protected $connection = 'ahmedabad_connection';
     protected $table = 'ward';
    protected $fillable=['name','admin_added'];
    protected $guarded=['id'];
}
