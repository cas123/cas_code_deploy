<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSessionData extends Model
{
    //
    use SoftDeletes;
    protected $connection = 'ahmedabad_connection';
    protected $table = 'user_session_data';
    protected $fillable = ['user_id','session_id'];
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

}
