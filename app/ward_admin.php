<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ward_admin extends Model
{
    //
    protected $connection = 'ahmedabad_connection';
     protected $table = 'ward_admin';
    protected $fillable=['ward_id','name','email_id','mail_sent'];
    protected $guarded=['id'];
}
