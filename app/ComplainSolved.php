<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplainSolved extends Model
{
    //
      protected $connection = 'ahmedabad_connection';
      protected $table = 'complain_solved';
      protected $fillable = ['complain_id','user_Satisfaction','proof_doc'];
      protected $guarded = ['id','created_at','updated_at'];

}
