<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    //
     protected $connection = 'ahmedabad_connection';
     protected $table = 'department';
    protected $fillable=['name','ward_id','admin_added'];
    protected $guarded=['id'];
}
