<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Mail;
use App\Employee;
class SendMailToWard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'checks ward table if entry exists then send the email to them for authentication every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

                 //get all the ward admins given by the municipal and send them the login link and authentication details so that they can add respective department admins 

            //check if ward table is emppty or not
          $countward = \App\ward_admin :: all()->count();
         if($countward > 0)
         {
            //municipal has given the list of ward admins

             $wardadmin = \App\ward_admin :: where('mail_sent','=',0)->first();

             if($wardadmin)
             {

             //generate password to send to ward admins
             $warspassword = Str :: random(7);

             //send mail to given mail id 

             $data = array('name' => $wardadmin['name'] , 'password' => $warspassword , 'email_id' => $wardadmin['email_id']);

             $sub = "log in credentials as ward admin";
             \Mail::send( 'wardmail',$data, function($message) use ($wardadmin,$sub){
             $message->to($wardadmin['email_id'], $wardadmin['name'])->subject
             ($sub);
             $message->from("cas276936@gmail.com","Civil Administration System");
           
             });

            //update mail_sent=1 in table


               $wardupdate = \App\ward_admin :: find($wardadmin['id']);
               $wardupdate->timestamps = false;
               $wardupdate->mail_sent = 1;
               $wardupdate->save();


               // insert to the employee_table

              $insertdata = array();
              $insertdata['password'] = $warspassword;
              $insertdata['email_id'] = $wardadmin['email_id'];
              $insertdata['name'] = $wardadmin['name'];
              $insertdata['is_wardAdmin'] = 1;
              $insertdata['is_departmentAdmin'] = 0;
              $insertdata['is_AddedAuthority'] = 0;
              $insertdata['notification_count'] = 0;
              $insertdata['notification_flag']=0;

              //if ward admin then area_id == ward_id
              $insertdata['area_id']=$wardadmin['ward_id'];
              //if ward admin then department_id has to be zero because he is not related to any department he is the overall head

              $insertdata['department_id']=0;

              \DB::connection('ahmedabad_connection')->table('employee')->insert($insertdata);

              //get the area_id 
              



               


             }






             


         }


    }
}
