<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\ward_admin;
use Mail;
use Illuminate\Support\Str;
use App\Employee_Notification;
use DB;
use App\Complains;
use App\Employee;
use App\ComplainSubmittedTo;
class AllocateComplains extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'allocate:complains';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'allocate complains of the user register to particular employee every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //logic for allocating compalians to particula employeees of the municipals
        //get the first complain whose status == pending and allocate it
       
           
            $complain =  \App\Complains :: where(function ($query) {
    $query->where('ward_id', '=', 1)
          ->orWhere('ward_id', '=', 2);
})->where('status','=','pending')->first();
          if($complain)
      {
          //allocate complain
          // get the area where the complain is associated
          //get the department of complain of the complain
          //assign the complain to particular employee of the ward //from the area we can get the ward it belongs to 


          $area_id = $complain['area_id'];
          $department_id = $complain['department_id'];
          //get the ward_id of the complain area

          $ward = \App\area :: select('ward_id')->where('id','=',$area_id)->first();
          $ward_id = $ward['ward_id']; 
      
          $employees = \App\Employee :: where('ward_id','=',$ward_id)->where('department_id','=',$department_id)->where('is_AddedAuthority','=',1)->get()->toArray();


          // choose randomly any employeee
          
          //*note for future word load balancing feature is to be added -- future params

          $employee_index = rand(0,sizeof($employees)-1);

          //get the employee id from employee_index

          $employee_id = $employees[$employee_index]['id'];

          $submitcomplain = array('complain_id' => $complain['id'],'employee_id' => $employee_id , 'from_user_id' => $complain['user_id']   );
           DB::beginTransaction();

          $insertresult = \App\ComplainSubmittedTo :: create($submitcomplain);
          if($insertresult)
          {


            //As Complain is assigned to particular employee  
            //update the status of complain to allocated to employee

            $change_complain_status = \App\Complains :: find($complain['id']);
            $change_complain_status->status = "complain allocated";

            if($change_complain_status->save())
            {
                DB::commit();


                //send the notification that complain has been allocated to employee

                //insert in to notification table employee_notification
                
                $notification_data = array();
                $notification_data['from_user_id']=$complain['user_id'];
                $notification_data['to_employee_id']=$employee_id;
                $notification_data['content']='you have been';
                $notification_data['type']='allocated complain with complain id='.' '.$complain['id'];
                $notification_data['is_read']=0;


                $notification_result = \App\Employee_Notification :: create($notification_data);
                
                if($notification_result)
                {


                    return "success";
                }  else
                {


                   return "error_notification";
                }





                 


            }else
            {

               DB :: rollback();
                return "failure";
            }


             

          }else
          {
             DB::rollback();
             return "failure";
          }
          

          



           

      }else
      {

           //all complains have been allocated

      }



    }
}
