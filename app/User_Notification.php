<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Notification extends Model
{
    //
     protected $connection = 'ahmedabad_connection';
      protected $table = 'user_notification';
      protected $fillable = ['to_user_id','from_employee_id','content','type','is_read'];
      protected $guarded = ['id','created_at','updated_at'];

}
