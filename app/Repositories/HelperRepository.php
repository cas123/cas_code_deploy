<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use App\Repositories\HelperRepositoryInterface;
use Mail;

class HelperRepository implements HelperRepositoryInterface
{
   /*
    Author : pravir pal
    input : void 
    output : random Key generated in form of string

    */
    public function generateKey() 
    {
    
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++)
    	{
          	$n = rand(0, $alphaLength);
         	$pass[] = $alphabet[$n];
    	}
    
    return implode($pass); //turn the array into a string
    
    }
    public function sendKey($data,$flag)
    {


           
    	    if(isset($data['employee_id']))
             {

               $employee_id = $data['employee_id'];

               $userdataget = \App\SuperEmployees :: where('id','=',$employee_id)->get();
               $userdata    = $userdataget->first();


               if(sizeof($userdataget) > 0)
               {
               	   //code to send password to registered
               	    $requiredarray = array("name" => $userdata['name'],"password" => $userdata['password'],"email_id" => $userdata['email_id']);
               	    return $this->sendMail($requiredarray,$flag);
               	    
               	    
               	  
               }else
               {
               	   return "invalid email_id";
      	            
               }
               
             }
             else
             {
             if(isset($data['email_id']))
             {


                      $email_id = $data['email'];

               $userdataget = \App\SuperEmployees :: where('email_id','=',$email_id)->get();
               $userdata    = $userdataget->first();


               if(sizeof($userdataget) > 0)
               {
                   //code to send password to registered
                    $requiredarray = array("name" => $userdata['name'],"password" => $userdata['password'],"email_id" => $userdata['email_id']);
                    return $this->sendMail($requiredarray,$flag);
                    
                    
                  
               }else
               {
                   return "invalid email_id";
                    
               }
               


             }else
             {
             	return "error couldnot find required data";
             }
             }
            
             
             
    }
    public function sendMail($userdata,$flag)
    {

    	try
    	{
          $sub="";
    	  if($flag == "forgotpassword")
    	  {
    	    $data = array('name'=> $userdata['name'],'password' => $userdata['password']);
    	    $sub="generated password";
          }
          if($flag == "firstmail")
          {
          	$data = array('name'=> $userdata['name'],'password' => $userdata['password'],'email_id' =>$userdata['email_id']);

          	$sub = "Registeration email verification";
          }
          
         
             \Mail::send( 'mail',$data, function($message) use ($userdata,$sub){
             $message->to($userdata['email_id'], $userdata['name'])->subject
             ($sub);
             $message->from("cas276936@gmail.com","Civil Administration System");
           
             });
             return "success";
           
         }
         catch(\Exception $e)
         {
              
              
            return "error";
               

         }
     }
   

}


