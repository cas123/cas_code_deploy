<?php

namespace App\Repositories;
use Illuminate\Http\Request;

interface HelperRepositoryInterface
{
	public function generateKey();
	public function sendKey($data,$flag);
	public function sendMail($userdata,$flag);
	

}