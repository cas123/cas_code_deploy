<?php


namespace App\Repositories;

class SuperMonthTrend {


        public function getSuperMonthAnalysis()
        {


         //                     $get_data = $request->all();
         
           //$ward_id   = $get_data['ward_id'];


                     
            $ward_result = \App\ward :: select('id','name')->get()->toArray();

           //select all department corresponsing to particuar ward

            
            $final_data_array=array();
            $final_outer_array=array();
            $final_inner_array =array();

           for($month=0;$month<=11;$month++)
           { 
                   
            for($ward_counter = 0;$ward_counter < sizeof($ward_result);$ward_counter++)
            {




            $department_result = \App\department :: select('id','name')->where('ward_id','=',$ward_result[$ward_counter]['id'])->get()->toArray();


            
            
            
            for($i=0;$i<sizeof($department_result);$i++)
            {

               $department_array[$i]=$department_result[$i]['id'];
            }
           
           // $area_result = \App\area :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();


            // $area_array=array();
            // for($i=0;$i<sizeof($area_result);$i++)
            // {

            //    $area_array[$i]=$area_result[$i]['id'];
            // }
            
              $department_ids = join("','",$department_array);  

 $result =  \DB::connection('ahmedabad_connection')->select(" select
        DISTINCT m1.department_id,m1.status, (select count(*)  from complains m2 where m2.status = m1.status and m1.department_id =m2.department_id) as total from
         complains m1 where department_id IN ('$department_ids') AND  MONTH(created_at) = ($month+1) AND YEAR(created_at) = 2019");
                 
     //  return $result;
         //$result contains values in the format department id,status,total as array of json objects


         //want to find the total satisfied and unsatisfied in each department


         //find all the complain ids of each particular department in ward from department_array

         $result_complain_ids = \App\Complains :: select('department_id','id')->whereIn('department_id',$department_array)->get()->toArray();

          $department_array_length = sizeof($department_array);

          $depart_assoc = array();
           

          for($i=0;$i<$department_array_length;$i++)
          {
             $temp_array= array();

          for($j=0;$j < sizeof($result_complain_ids);$j++)
          {

                  if(isset($result_complain_ids[$j]['id']))
                  {
                     if($result_complain_ids[$j]['department_id'] == $department_array[$i])
                     {
                           $temp_array[$j] = $result_complain_ids[$j]['id'];
                     }
                  }

          }//end of inner for loop
          $depart_assoc[$department_array[$i]] = $temp_array;
          unset($temp_array);

        }//end of outer for loop

           $dept_user_array = array();
           
           $counter=0;
          foreach ($depart_assoc as $key => $val)
           {
              $dept_user_obj =  new \stdClass();
                
            $resultun = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',0)->get()->toArray();
            $resultsa = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',1)->get()->toArray(); 
            $len = sizeof($resultun);
            $len_satisfied = sizeof($resultsa);
            $dept_user_obj->department_id=$key;
            $dept_user_obj->user_unsatisfied=$len;
            $dept_user_obj->user_satisfied=$len_satisfied;
           
            $dept_user_array[$counter] = $dept_user_obj;
            array_push($result,$dept_user_obj);
             $counter=$counter+1;
             unset($dept_user_obj);
          
           } //end of for loop

           $final_depart_obj =  array();

           //get the names of the department

           $result_name = \App\department :: whereIn('id',$department_array)->get()->toArray();
           
           //prepare final object
           //$counter=0;
             $resultant_array=array('solved' => 0,'unsolved' => 0,'allocated_complains' => 0,'pending' => 0,'user_satisfaction_count'=> 0,'user_unsatisfaction_count' => 0,'total_complains' => 0);


               
           for($i=0;$i<sizeof($result_name);$i++)
           {

              //make an array to associate each key in object to array of required values
          
              for($j=0;$j<sizeof($result);$j++)
              {

                 if($result[$j]->department_id == $result_name[$i]['id'])
                 {

                    if(!isset($result[$j]->status) )
                    {

                         $resultant_array["user_satisfaction_count"] += $result[$j]->user_satisfied;
                         $resultant_array["user_unsatisfaction_count"] += $result[$j]->user_unsatisfied; 

                    }else
                    {

                    if($result[$j]->status == "pending")
              
                          $resultant_array['pending'] += $result[$j]->total;
                    elseif ($result[$j]->status == "complain allocated") 
                             $resultant_array['allocated_complains'] += $result[$j]->total;
                    elseif($result[$j]->status == "solved")

                            $resultant_array['solved'] += $result[$j]->total;

                    elseif($result[$j]->status == "unsolved")

                          $resultant_array['unsolved'] += $result[$j]->total;
                    
              
            
                   }
              
              
               }
          }//end of inner for loop
           
        }//end of outer for loop
           //calculate total/complains , total_solved ,total_unsolved,total pending,total allocated ,user unsatosfaction count for each ward
        $resultant_array["total_complains"] += ($resultant_array['solved']+
                                                    $resultant_array['unsolved']+
                                                    $resultant_array['pending']+
                                                    $resultant_array['allocated_complains']);

                       
            $resultant_array["ward_name"] = $ward_result[$ward_counter]['name'];
            $resultant_array["ward_id"] = $ward_result[$ward_counter]['id'];


       

            array_push($final_inner_array, $resultant_array);


         
            unset($resultant_array);


     }
        


          array_push($final_outer_array, $final_inner_array);
         
      
   
         array_push($final_data_array, $final_outer_array);
        
        $final_inner_array=[];
   
     }
       return $final_outer_array;

     }

   

}



