<?php

namespace App\Repositories\SuperAdminRepository;
use  Illuminate\Http\Request;
interface SuperAdminRegisterInterface 
{
	public function registerSuperAdmin($requestdata);
}