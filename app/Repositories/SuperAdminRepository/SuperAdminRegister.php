<?php
namespace App\Repositories\SuperAdminRepository;
use Illuminate\Http\Request;
use App\Repositories\SuperAdminRepository\SuperAdminRegisterInterface;
use App\SuperEmployees;
use App\Repositories\HelperRepositoryInterface;
use App\Repositories\HelperRepository;
use App\RegisteredMunicipals;

class SuperAdminRegister implements SuperAdminRegisterInterface
{
    private $generatekey;
	public function __construct(HelperRepositoryInterface $generatekey)
	{
		$this->generatekey = $generatekey;

	}
	/*
	author : pravir pal
	input  : registerdata
	output : boolean
	*/
	public function  registerSuperAdmin($registerdata)
	{
		 /*
		 (skip/optional)step 1 : validate all the attribute  //to be done later
		 step 2 : create entry in super_employees table 
		      i) generate password for super_employee
		      ii)save the data to table
		 */
         //step 2
		 $password = $this->generatekey->generateKey();
         $registerdata['password']=$password;
         unset($registerdata['secretkey']);
         $result = \App\SuperEmployees :: create($registerdata);
         return $result;


	}
    public function registerMunicipal($registermunicipal,$employee_id)
    {   

        
         $activation_token = $this->generatekey->generateKey();
         $registermunicipal["employee_id"] = $employee_id;
         $registermunicipal["activation_token"] = $activation_token;
         $registermunicipal["is_verified"] = 0;
         $result = \App\RegisteredMunicipals :: create($registermunicipal);
         if($result)
         {
         	return "success";
         }else
         {
         	return "error_municipal";
         }





    }



}