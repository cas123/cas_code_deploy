<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Str;

class AuthorityController extends Controller
{
    //

    public function logIn(Request $request)
    {

              $data = $request->all();

             return view('authdash',compact('data'));
 

    }
    public function fetchComplains(Request $request)
    { 



            $get_data = $request->all();

            $employee_id = $get_data['employee_id'];

            $get_complains = \DB::connection('ahmedabad_connection')
            ->table('complain_submitted_to')
            ->select('complain_submitted_to.complain_id as complain_id','complain_submitted_to.employee_id as employee_id','complain_submitted_to.from_user_id as from_user_id','complains.created_at as created_at','complain_solved.user_satisfaction as user_satisfaction','complains.description as description','complains.photo as photo','complains.video as video', \DB::raw("(CASE WHEN Complains.status = 'complain allocated' THEN 1 WHEN complains.status='unsolved' THEN 2 WHEN complains.status='solved' THEN 3 END) AS status"),'complains.type as department_name','complains.area_id as area_id','complains.department_id as department_id','complains.latitude as latitude','complains.longitude as logitude','users.first_name as user_first_name','users.last_name as user_last_name','users.email as user_email','users.phone as user_contact','users.gender as user_gender','users.profile_image as user_profile_image','users.address as user_address')
            ->leftjoin('complain_solved','complain_submitted_to.complain_id','=','complain_solved.complain_id')
            ->join('complains','complains.id','=','complain_submitted_to.complain_id')
            ->join('users','complain_submitted_to.from_user_id','=','users.id')
            ->where('complain_submitted_to.employee_id','=',$employee_id)
            ->where('complains.status','!=',"pending")
            ->get()
            ->toArray();

            //converting to required format for front end 


         $return_data =  new \stdClass();

          $total = sizeof($get_complains);
         $sort  ="asc";
         $field = "user_first_name";
         $page = 1;
         $pages = 1;
         $perpage = 10;


         $meta = array(

                 "page" => $page,
                 "pages" => $pages,
                 "perpage" => $perpage,
                 "total" => $total,
                 "sort"  => $sort,
                 "field"  => $field
         );

         $return_data->meta = $meta;
         $return_data->data = $get_complains;



         

        return json_encode($return_data);


           // return $get_complains;


    }



     public function sendNotification(Request $request)
     {

                   if(isset($_POST["view"]))
                 {

                 	$employee = $request->all();
                     $employee_id = $employee['employee_id'];
 

 $connect = mysqli_connect("localhost", "root", "", "ahmedabad");
  
 if($_POST["view"] != '')
 {
   $update_query = "UPDATE employee_notification SET is_read=1 WHERE is_read=0 and to_employee_id=$employee_id";
  mysqli_query($connect, $update_query);
 }
 $query = "SELECT * FROM employee_notification where to_employee_id=$employee_id  ORDER BY id DESC";
 $result = mysqli_query($connect, $query);
 //dd($query);
 $output = '';
 
 if(mysqli_num_rows($result) > 0)
 {


 	//fetch the user name from user_id


  while($row = mysqli_fetch_array($result))
  {
  	 $user_result = \App\Users :: select('first_name')->where('id','=',$row["from_user_id"])->first();
  	 $user_name = $user_result['first_name'];
   $output .= '
   <li>
    <a href="#">
     <strong>'.'from'.' '.$user_name.'-:'.'</strong><br />
     <small><em>'.$row["content"]." ".$row["type"].'</em></small>
     <br><label>at-:'. $row['created_at'] .'</label>
    </a>
   </li>
   <li class="divider"></li>
   ';
  }
 }
 else
 {
  $output .= '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
 }
 
 $query_1 = "SELECT * FROM employee_notification WHERE is_read=0 and to_employee_id=$employee_id";
 $result_1 = mysqli_query($connect, $query_1);
 $count = mysqli_num_rows($result_1);
 $data = array(
  'notification'   => $output,
  'unseen_notification' => $count
 );
 
}



    return json_encode($data);




     }



     public function getSolutionProof(Request $request)
     {


     	      $request_data = $request->all();
     	      $complain_id = $request_data['complain_id'];
     	      $user_satisfaction=0;

     	      //$proofdoc
        
          $check_complain_solved = \App\Complains :: select('id')->where('id','=',$complain_id)->where('status','=','solved')->first();
          if($check_complain_solved)
          {
             return "exists";
          } 

             if(isset($_FILES['fileToUpload']))
             {
                

             	   $target_dir = "c:\\xampp\htdocs\ahmedabad_solution_proof\\";
$target_file = $target_dir .  basename($_FILES["fileToUpload"]["name"]);


// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    return -1;
}

// Check if $uploadOk is set to 0 by an error
	   //return $target_file;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      //  echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
      
       $proof_doc ="http://cas.mindhackers.org/ahmedabad_solution_proof/". $_FILES["fileToUpload"]["name"];

      $resultant_array = array();
      $resultant_array['complain_id'] = $complain_id;
      $resultant_array['user_Satisfaction']=0;
      $resultant_array['proof_doc']=$proof_doc;
      try
      {
      \DB::beginTransaction();

         //check if complain id is vald

       	   $result_id = \App\Complains :: select('id','user_id','created_at')->where('id','=',$complain_id)->first();
       	   if(!$result_id)
       	   {

       	   	  return -1;

       	   }



      $result_insert = \App\ComplainSolved :: create($resultant_array);
       if($result_insert)
       {

       	    //update status in complains table


       	   $us = \App\Complains :: find($complain_id);
           $us->status="solved";
       	     if($us->save())
       	   {

              //generate certificate for user $result_id['user_id']
            try{

                     $target_dir_bb = "c:\\xampp\htdocs\aest";
                    $target_file_bb = $target_dir_bb . DIRECTORY_SEPARATOR . basename("bb.jpg");
                    $target_dir_b2 = "c:\\xampp\htdocs\aest";
                    $target_file_b2 = $target_dir_b2 . DIRECTORY_SEPARATOR . basename("b2.jpg");
     
      
           $cert_user =  \App\Users :: select('first_name','last_name','profile_image')->where('id','=',$result_id['user_id'])->first();
           //get the authority name
           $auth_result = \App\ComplainSubmittedTo :: select('employee_id')->where('complain_id','=',$complain_id)->first();
           $emp_result = \App\Employee :: select('name')->where('id','=',$auth_result['employee_id'])->first();

           $getsolveddate = \App\ComplainSolved :: select('created_at')->where('complain_id','=',$complain_id)->first();

           
            $required_data = array('first_name'=>$cert_user['first_name'],'last_name' => $cert_user['last_name'],

                                    'profile_image' => $cert_user['profile_image'],'created_at'=>$result_id['created_at'],
                                    'solved_at' => $getsolveddate['created_at'],
                                    'auth_name' => $emp_result['name'],
                                    'complain_id' => $complain_id,
                                    'bb' => $target_file_bb,
                                    'b2' =>  $target_file_b2

                                  );

            $output = $this->generateHTML($required_data);
           }catch(\Exception $e)
           {

                //continue if exception occurs because certification genratio is optional things

           }

             //generetae certificate link
             try{
              $random = Str :: random(8);

            $target_dir = "c:\\xampp\htdocs\ahmedabad_certificates";
            $target_file = $target_dir . DIRECTORY_SEPARATOR . basename("mycerti_$random.pdf");
            PDF::loadHtml($output)->save($target_file);
            $target_file="http:\\cas.mindhackers.org\ahmedabad_certificates\mycerti_$random.pdf";
            }catch(\Exception $e)
            {
               // return $e;
                $target_file="Certificate generation is in process";

            }

            $certificate_array = array('user_id' => $result_id['user_id'],'complain_id' => $complain_id,'certificate_link'=>$target_file);
            try{
            $result_certi = \App\GeneratedCertificate :: create($certificate_array);
            }catch(\Exception $e)
            {
              
            }
       	   	    \DB::commit();



                   //get token with respect to user

       	   	    $token_result = \App\Users :: select('instance_id','first_name')->where('id','=',$result_id['user_id'])->first();
                $token =$token_result['instance_id'];

                 $mess_user =  \App\Users :: select('first_name','email')->where('id','=',$result_id['user_id'])->first();
                 $name = $mess_user['first_name'];
                $message = "Hello $name !,your complain with id"."=".$complain_id." "."has been solved.please give required feedback after looking at the solution report which is available at complain tracking tab in CAS App";

                $certificate_message = "Hello $name !,your appreciation certificate has been succesfully generated.you can download it from our CAS App";

                 //save this to user_notification table


                $from_employee_id=0;
                $to_user_id=$result_id['user_id'];
                $content =$message;
                $type="system generated";
                $is_read=0;

                $insert_array = array('from_employee_id'=>0,'to_user_id'=>$to_user_id,'content'=>$message,'type'=>$type,'is_read'=>0);


                $insert_noti = \App\User_Notification :: create($insert_array);

                

       	   	    //fire notification that complain with some is solved
                $send_data;
                try{

                  $send_data = $this->sendNotificationToUser($result_id['user_id'],$token,$message);

                }catch(\Exception $e)
                {

                 

                }try{

                   $send_data = $this->sendNotificationToUser($result_id['user_id'],$token,$certificate_message);

                }catch(\Exception $e)
                {


                }



                try{
                   
                   $this->sendEmail($message,$mess_user['email'],"Complain Solved");

                }catch(\Exception $e)
                {

                }
                try{
                  $this->sendEmail($certificate_message,$mess_user['email'],"Certificate Generation");

                }catch(\Exception $e)
                {

                }
                
               
       	   	    return $send_data;
       	   }else
       	   {

       	   	   \DB::rollback();
       	   	   return -1;
       	   }


       }else
       {

       	   return -1;
       }
   }catch(\Exception $e)
   {

   	   \DB::rollback();
   	   return $e;
   }

       

        


    } else {
       return -1;
    }


}
         else
         {

        
         	 return -1;
         }


     }


   public function generateHTML($cert_data)
   {
   

        return "<!DOCTYPE html>
<html>
<head>
  <title>Certificate</title>
  <style type='text/css'>
    body{
      background-image: url('http://cas.mindhackers.org/aest/bb.jpg');
      
    }
    page {
        background-image: url('http://cas.mindhackers.org/aest/b2.jpg');            
        background-repeat: no-repeat;
        background-size: contain;

        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
      }
      page[size='A4'][layout='portrait'] {
        width: 29.7cm;
        height: 17.4cm;  
      }
    #icon{
      float: left; 
      margin-top: 20px; 
      margin-left: 350px;
      margin-bottom: 10px;
      margin-right: 20px;
      border-radius: 80px;
    }
    #text1{
      text-align: center;
    }
    #profileImg{
      margin: 25px;
      float: left;
    }
    #content{
      margin-top: 60px;
      
      margin-right: 80px;
    }
    #bg-text
    {
        color:linen;
        font-size:120px;
        transform:rotate(300deg);
        -webkit-transform:rotate(300deg);
    }

    #title{
      padding: 35px;
      font-family: verdana;
    }
    #watermark
    {
     position:fixed;
     bottom:5px;
     right:5px;
     opacity:0.5;
     z-index:99;
     color:white;
    }
    #sign{
      margin-top: 50px;
      margin-left: 90px;
      float: left;
    }
    #cas{
      margin-top: 50px;
      margin-right: 350px;
      float: right;
      margin-left: 20px

    }

    #signnn{
      float: right;
      border-radius: 40px;

    }
  </style>
</head>
<body>
<page size='A4' layout='landscape'>

  
  <img src='http://cas.mindhackers.org/aest/icon.jpg' width='120' height='120' id='icon'>
  <h2 id='title'>Civil<br> Administration<br> System </h2>
  <hr width='800px'>
  <h2 id='text1'>Certificate Of Appreciation</h2>
  <hr width='800px'>
  
  <img src='". $cert_data['profile_image']  . "' width='200px' height='200px' id='profileImg'>
  <h2 id='content'>This certificate is presented to ".$cert_data['first_name']." " . $cert_data['last_name']." for making an effort to register complain with complain id=".$cert_data['complain_id']." on". $cert_data['created_at']  .  "and was successfully solved by our authority on "." ".$cert_data['solved_at']. " </h2>&nbsp;&nbsp;&nbsp;<h3 id='sign'>". $cert_data['auth_name'].",CAS Authority<br></h3>
  <h3 id='cas'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CAS</h3>
  
<div>
 </div>
  
</page>
</body>
</html>";





   }



     public function sendNotificationToUser($user_id,$token,$message)
     {

     	           $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
       // $token='fmt3ssjef7s:APA91bGuXerFVN4CkvAyvfQBvuMVcJrdmXy2rKjjeU36JvVmoyc0g2YR-Tpv8ByH84dWUpmcPTyQL68iqkZpis1Zta6hV6PiHwK2Z0KcjrMbaCKwR1gdegzNp0Oqyouoqd_LhPADaWRH';
    

    $notification = [
        'body' => $message,
        'sound' => false
    ];
    
    $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

    $fcmNotification = [
        //'registration_ids' => $tokenList, //multple token array
        'to'        => $token, //single token
        'notification' => $notification,
        'data' => $extraNotificationData
    ];

    $headers = [
        'Authorization: key=AIzaSyC4XQO8kHhHcE3UfE1pgrEuVMHI_sCE2kY',
        'Content-Type: application/json'
    ];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = json_decode(curl_exec($ch));
    curl_close($ch);
    
   
   $result->user_message = $message;


    
     return json_encode($result);
 



     }


    public function sendEmail($message,$email,$subject)
    {

              $viewdata=array();
              $viewdata['content']=$message;
              $viewdata['email_id']=$email;
              $sub=$subject;
              \Mail::send( 'usertemplatemail',$viewdata, function($message) use ($viewdata,$sub){
              $message->to($viewdata['email_id'])->subject
              ($sub);
              $message->from("cas276936@gmail.com","Civil Administration System");
             

              });
 


    }


       public function sendPassword(Request $request)
         {

                         $request_data = $request->all();
              if(isset($request_data['email_id']))
              {
                  $email_id = $request_data['email_id'];
                  

                  $password_result = \App\Employee :: select('name','password')->where('email_id','=',$email_id)->first();
                  if($password_result)
                  {
                         $user_name = $password_result['name'];
                         $password = $password_result['password'];

                         //send email to $user_name with $email_id
                   $data= array();
                   $data['name']=$user_name;
                   $data['email_id']=$email_id;
                   $data['password']=$password;
                   

                   $sub="forgot password";

                   try
                   {

              \Mail::send( 'usermail',$data, function($message) use ($data,$sub){
             $message->to($data['email_id'], $data['name'])->subject
             ($sub);
             $message->from("cas276936@gmail.com","Civil Administration System");
           
             });

      
             return "success";
           }catch(\Exception $e)
           {
                return -1;
           }


                  }else
                  {
                     return -1;
                  }

              }else
              {

                  return -1;
              }




         }



}











