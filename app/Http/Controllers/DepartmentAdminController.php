<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\EmployeeSessionData;

class DepartmentAdminController extends Controller
{
    

    

    
    public function getAnalytics(Request $request)
    {
    	 //get the admin's department

         $get_data = $request->all();
         
         $department_id = $get_data['department_id'];

           //fetch all the employees corresponding to the particular department

         $employees = \App\Employee :: where('department_id','=',$department_id)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->get()->toArray();
         //get the id of the employees to bind it to single dimensional array

         $employeearray = [];
         for($i=0;$i<sizeof($employees);$i++){
            $employeearray[$i] = $employees[$i]['id'];
         }


         $analysis_type = 0;

         if(isset($get_data['year']) && isset($get_data['month']))
         {

             $analysis_type=1;
         }
                

         //check all the complains associated with the particular department employees i.e get all the complain id's associated with the particular employees with its status and user satisfaction of the solution

         if($analysis_type == 1)
         {

            $result=  DB::connection('ahmedabad_connection')
                  ->table('complain_submitted_to')
                 ->select('complain_submitted_to.complain_id','employee.name','complain_submitted_to.employee_id','complains.area_id','area.name as area_name','complains.status','complain_solved.user_satisfaction','complains.created_at')
                 ->leftjoin('complain_solved', 'complain_solved.complain_id', '=', 'complain_submitted_to.complain_id')
                 ->join('complains', 'complains.id', '=', 'complain_submitted_to.complain_id')
                 ->join('employee','complain_submitted_to.employee_id','=','employee.id')
                 ->join('area','area.id','=','complains.area_id')
                 ->where('complains.department_id','=',$department_id)
                 ->whereIn('complain_submitted_to.employee_id', $employeearray)
                 ->whereYear('complains.created_at','=',$get_data['year'])
                 ->whereMonth('complains.created_at','=',$get_data['month'])
                 ->get()
                 ->toArray();
                 return $result;
          }
          if($analysis_type == 0)
          {
                       

                  $result_emp =  DB::connection('ahmedabad_connection')
                  ->table('complain_submitted_to')
                 ->select('complain_submitted_to.complain_id','employee.name','complain_submitted_to.employee_id','complains.area_id','area.name as area_name','complains.status','complain_solved.user_satisfaction','complains.created_at')
                 ->leftjoin('complain_solved', 'complain_solved.complain_id', '=', 'complain_submitted_to.complain_id')
                 ->join('complains', 'complains.id', '=', 'complain_submitted_to.complain_id')
                 ->join('employee','complain_submitted_to.employee_id','=','employee.id')
                 ->join('area','area.id','=','complains.area_id')
                 ->where('complains.department_id','=',$department_id)
                 ->whereIn('complain_submitted_to.employee_id', $employeearray)
                 ->get()
                 ->toArray();
                 
      


              return $result_emp;


          }


        
                   

    }

    public function getEmployees(Request $request)
    {

    	 //get the admin's department

         $get_data = $request->all();
         $department_id = $get_data['department_id'];

           //fetch all the employees corresponding to the particular department

         $employees = \App\Employee :: where('department_id','=',$department_id)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->where('is_AddedAuthority','=',0)->get()->toArray();
         return $employees;


    }

    public function getAddedAuthorities(Request $request)
    {

              //get all the added employees

          $get_data = $request->all();
          $department_id = $get_data['department_id'];

           //fetch all the employees corresponding to the particular department

         $employees = \App\Employee :: where('department_id','=',$department_id)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->where('is_AddedAuthority','=',1)->get()->toArray();
      
           
         //converting to required format

         $return_data =  new \stdClass();

         $total = sizeof($employees);
         $sort  ="asc";
         $field = "name";
         $page = 1;
         $pages = 1;
         $perpage = 10;


         $meta = array(

                 "page" => $page,
                 "pages" => $pages,
                 "perpage" => $perpage,
                 "total" => $total,
                 "sort"  => $sort,
                 "field"  => $field
         );

         $return_data->meta = $meta;
         $return_data->data = $employees;



         

        return json_encode($return_data);

         
        




    }
    public function LogIn(Request $request)
    {
    	$data = $request->all();

    	return view('deptadmindash',compact('data'));
    }

  

    public function LogOut(request $request)
    {
          
    	   $admindata = $request->all();

             //check if you are not logged in
             $admin  = \App\EmployeeSessionData::where('session_id','=',$admindata['session_id'])->where('user_id','=',$admindata['id'])->where('deleted_at','=',null)->get()->toArray();
             
             
              if(!empty($admin[0]) && $admin[0]['deleted_at']==null){
             


                     $destroy = \App\EmployeeSessionData::destroy($admin[0]['id']);
             

                     return "successfully logged out";


              }else
              {
                return "your session is not created yet";
              }

             
     }

    /*
    @author : pravir pal
    @input  : request object containing all the requested employees to be added
    @output : successful insertion or failure

    */
     public function addAuthority(Request $request)
     {
                  

                   $data = $request->all();
                   $data_array = $data["param"]; 

                

                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                   DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_AddedAuthority = CASE is_AddedAuthority 
                     WHEN 0 then 1 
                     WHEN 1 THEN 1 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );
                   

                    //send mail to every added authority
                              

                       $viewdata= array();
                       $viewdata['role']="Authority";

                       $get_detail = \App\Employee :: select('name','email_id','password')->where('id','=',$data_array[$i])->where('is_AddedAuthority','=',1)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->first();

                       if($get_detail)
                       {

                       $viewdata['name']=$get_detail['name'];

                       $viewdata['email_id']=$get_detail['email_id'];

                       $viewdata['password']=$get_detail['password'];


                      try
                  {
                     $sub="CAS PROCEDURE";
              
             
          
               dispatch(new \App\Jobs\SendEmailForAdd($viewdata));


             // \Mail::send( 'AddTemplate',$viewdata, function($message) use ($viewdata,$sub){
             // $message->to($viewdata['email_id'], $viewdata['name'])->subject
             // ($sub);
             // $message->from("cas276936@gmail.com","Civil Administration System");
             

             // });
             
           
           }
            catch(\Exception $e)
           {
              
              
            
               return $e;

          }
        }
  





            

                   }


                     

                       return "success";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }


     	  


         
     }

     public function removeAuthority(Request $request)
     {

             $data = $request->all();
                   $data_array = $data["param"]; 
                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                   DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_AddedAuthority = CASE is_AddedAuthority 
                     WHEN 0 then 0 
                     WHEN 1 THEN 0 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );

                      
                            $result_employee = \App\Employee :: select('name','department_id','email_id')->where('id','=',$data_array[$i])->first();
                    

                     if($result_employee)
                     {
                     
                        $department_id = $result_employee['department_id'];


                     
                   
                          $viewdata=array();
                          $viewdata['email_id']=$result_employee['email_id'];
                          $viewdata['role']  = "DepartmentAdmin";
                          $viewdata['name'] = $result_employee['name'];

                          dispatch(new \App\Jobs\SendEmailForRemove($viewdata));
                      }

                     















                   }

                    return "success";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }





     }

     public function getTopAreas(Request $request)
     {
                $request_data = $request->all();
                $department_id = $request_data['department_id'];
           //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('area.name',\DB::raw("count(area_id) as total_complains"))->join('area','complains.area_id','=','area.id')->where('complains.department_id','=',$department_id)->groupBy('area.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }

     

}











