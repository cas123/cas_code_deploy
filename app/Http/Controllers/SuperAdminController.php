<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\RegisteredMunicipals;
use App\Repositories\SuperAdminRepository\SuperAdminRegisterInterface;
use DB;
use Exception;
use App\Municipal;
use App\superEmployees;
use App\Repositories\HelperRepositoryInterface;
use App\SessionData;
use App\Repositories\SuperMonthTrend;
class SuperAdminController extends Controller
{
     private $superrepository;
     private $helper;
     public function __construct(SuperAdminRegisterInterface $superrepository,HelperRepositoryInterface $helper)
     {
     	$this->superrepository = $superrepository;
     	$this->helper=$helper;
     }
    //
     /*
     author : pravir pal
     input  : request object
     output : returns boolean

     */

    public function register(Request $request)
    {
          /*
          step 1 : verify the secret key given by municipal with the secret key in request object
          */
          //step 1
        try 
        {
          	
            DB::beginTransaction();
           	$registerdata = $request->all();
            $joindata = App\CodeVerify :: with('superEmployee')->first();
            $govsecretdata = $joindata->secretkey;
            $registersecretdata = $registerdata['secretkey'];
            if($govsecretdata == $registersecretdata)
            {
        	/*code to register superadmin  to our super_employees table and register corresponding municipal to registered_municipals table*/
        	$result = $this->superrepository->registerSuperAdmin($registerdata);
        	$employee_id = DB::getPdo()->lastInsertId();
        	if($result)
        	{
        		//code to register municipals
        		//find all the data required first and then create it in the table registered_municipals

        		$registermunicipal = $this->superrepository->registerMunicipal($registerdata,$employee_id);
                if($registermunicipal == "success")
        	    {

        	    	 //send account activation link message with corresponding user password to login
        	    	 $dataarray=["employee_id" => $employee_id];
        	    	 $result = $this->helper->sendKey($dataarray,"firstmail");
        	    	 if($result == "success")
        	    	 {

                        DB :: commit();
                         return "link is sent to your email..activate your account by clicking it";
                     }
                     else
                     {

                           DB ::rollback();
                     	 return "error in sending email..please contact customer care ";
                     }	
        	    } else
        	    {
        	    	DB :: rollBack();
        	    	return "error registering municipal...rolled back previous transaction";
        	    }
        		
        	}else
        	{
        		return "error registering super admin";
        	}
       }else
       {

       	   return "secret key do not match";
       }
    

       
        } 
         catch (Exception $e) 
        {
           DB :: rollBack();
           return " exception error=" . $e;  	
        }  
        
    	
    }
    public function getMunicipalTable()
    {
          
    	$municipaltable = App\Municipal :: get();
      
    	
    	if($municipaltable)
    	{
    		return $municipaltable;
    	}else
    	{
    		return "error";
    	}

    }
    /*
    takes request object containing employee_id and returns success for email sent to registered email else failure
    */
    public function forgotPassword(Request $request)
    {


    	     
             $data = $request->all();
             $result = $this->helper->sendKey($data,"forgotpassword");
             if($result == "success")
             {
             	return "email sent successfully";
             }else
             {
             	return "invalid  id";
             }
             



    }

    public function emailVerified($email_id)
    {
           try
           {





           date_default_timezone_set('Asia/Kolkata'); 
    	     $getdatetime = date("Y-m-d h:i:s"); 
           
    	     $result = App\SuperEmployees :: where('email_id','=',$email_id)->first();
           if($result)
           {
           if($result['email_verified_at'] != null)
           {
                 return "your account is already activated..please log in to continue";
           }
           $result->email_verified_at = $getdatetime;
           if($result->save())
           {
              return "account activated successfully";
           }else
           {
              return "error activating account.please conntact customer care";
           }
         }else
         {

              return "invalid email";
         }
           }catch(Exception $e)
           {
               return "exception error during activation of email = ".$e;
           }
           

    }
    /*
        input is request object 

    */
    public function adminLogin(Request $request)
    {
               $data = $request->all();

               return view('superadmindash',compact('data'));

               //return $request;

    }

      public function adminLogout(Request $request)
      {

             $admindata = $request->all();

             //check if you are not logged in
             $admin  = App\SessionData::where('session_id','=',$admindata['session_id'])->where('user_id','=',$admindata['user_id'])->get()->toArray();
             
              if(!empty($admin[0]) && $admin[0]['deleted_at']==null){
             


                     $destroy = App\SessionData::destroy($admin[0]['id']);
             

                     return "successfully logged out";


              }else
              {
                return "your session is not created yet";
              }

             
        }



        public function getWardWiseAnalysis(Request $request)
        {


         //                     $get_data = $request->all();
         
           //$ward_id   = $get_data['ward_id'];


                     
            $ward_result = \App\ward :: select('id','name')->get()->toArray();

           //select all department corresponsing to particuar ward

            $final_result_obj = array();

            for($ward_counter = 0;$ward_counter < sizeof($ward_result);$ward_counter++)
            {




            $department_result = \App\department :: select('id','name')->where('ward_id','=',$ward_result[$ward_counter]['id'])->get()->toArray();


            
            
            
            for($i=0;$i<sizeof($department_result);$i++)
            {

               $department_array[$i]=$department_result[$i]['id'];
            }
           
           // $area_result = \App\area :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();


            // $area_array=array();
            // for($i=0;$i<sizeof($area_result);$i++)
            // {

            //    $area_array[$i]=$area_result[$i]['id'];
            // }
            
              $department_ids = join("','",$department_array);  

         $result =  DB::connection('ahmedabad_connection')->select(" select
        DISTINCT m1.department_id,m1.status, (select count(*)  from complains m2 where m2.status = m1.status and m1.department_id =m2.department_id) as total from
         complains m1 where department_id IN ('$department_ids')");
         
     //  return $result;
         //$result contains values in the format department id,status,total as array of json objects


         //want to find the total satisfied and unsatisfied in each department


         //find all the complain ids of each particular department in ward from department_array

         $result_complain_ids = \App\Complains :: select('department_id','id')->whereIn('department_id',$department_array)->get()->toArray();

          $department_array_length = sizeof($department_array);

          $depart_assoc = array();
           

          for($i=0;$i<$department_array_length;$i++)
          {
             $temp_array= array();

          for($j=0;$j < sizeof($result_complain_ids);$j++)
          {

                  if(isset($result_complain_ids[$j]['id']))
                  {
                     if($result_complain_ids[$j]['department_id'] == $department_array[$i])
                     {
                           $temp_array[$j] = $result_complain_ids[$j]['id'];
                     }
                  }

          }
          $depart_assoc[$department_array[$i]] = $temp_array;
          unset($temp_array);

        }

           $dept_user_array = array();
           
           $counter=0;
          foreach ($depart_assoc as $key => $val)
           {
              $dept_user_obj =  new \stdClass();
                
            $resultun = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',0)->get()->toArray();
            $resultsa = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',1)->get()->toArray(); 
            $len = sizeof($resultun);
            $len_satisfied = sizeof($resultsa);
            $dept_user_obj->department_id=$key;
            $dept_user_obj->user_unsatisfied=$len;
            $dept_user_obj->user_satisfied=$len_satisfied;
           
            $dept_user_array[$counter] = $dept_user_obj;
            array_push($result,$dept_user_obj);
             $counter=$counter+1;
             unset($dept_user_obj);
          
           }

           $final_depart_obj =  array();

           //get the names of the department

           $result_name = \App\department :: whereIn('id',$department_array)->get()->toArray();
           
           //prepare final object
           //$counter=0;
             $resultant_array=array('solved' => 0,'unsolved' => 0,'allocated_complains' => 0,'pending' => 0,'user_satisfaction_count'=> 0,'user_unsatisfaction_count' => 0,'total_complains' => 0);


               
           for($i=0;$i<sizeof($result_name);$i++)
           {

              //make an array to associate each key in object to array of required values
          
              for($j=0;$j<sizeof($result);$j++)
              {

                 if($result[$j]->department_id == $result_name[$i]['id'])
                 {

                    if(!isset($result[$j]->status) )
                    {

                         $resultant_array["user_satisfaction_count"] += $result[$j]->user_satisfied;
                         $resultant_array["user_unsatisfaction_count"] += $result[$j]->user_unsatisfied; 

                    }else
                    {

                    if($result[$j]->status == "pending")
              
                          $resultant_array['pending'] += $result[$j]->total;
                    elseif ($result[$j]->status == "complain allocated") 
                             $resultant_array['allocated_complains'] += $result[$j]->total;
                    elseif($result[$j]->status == "solved")

                            $resultant_array['solved'] += $result[$j]->total;

                    elseif($result[$j]->status == "unsolved")

                          $resultant_array['unsolved'] += $result[$j]->total;
                    
              
            
             }
              
              
            }
          }//end of inner for loop
//          return $resultant_array;

              //     if(!isset($resultant_array['solved']))
              // {
              //       $resultant_array['solved']==0;
              // }
              //  if(!isset($resultant_array['usolved']))
              // {
              //       $resultant_array['usolved']==0;
              // }
              //  if(!isset($resultant_array['allocated_complains']))
              // {
              //       $resultant_array['allocated_complains']==0;
              // }
              //  if(!isset($resultant_array['pending']))
              // {
              //       $resultant_array['pending']==0;
              // }
             
                

                


              

              
             //  $resultant_array["department_name"]=$result_name[$i]['name'];
             //  $resultant_array["department_id"] = $result_name[$i]['id'];
             // //  return $resultant_array;
             // $final_depart_obj-> = $result_name['id'];
             // $final_depart_obj[$i] = $resultant_array;
             
                
         
           //unset($resultant_array);
           
        }//end of outer for loop



   
  //         $final_result_obj[$ward_result[$ward_counter]['name']] = $final_depart_obj;

           //calculate total/complains , total_solved ,total_unsolved,total pending,total allocated ,user unsatosfaction count for each ward
        $resultant_array["total_complains"] += ($resultant_array['solved']+
                                                    $resultant_array['unsolved']+
                                                    $resultant_array['pending']+
                                                    $resultant_array['allocated_complains']);

                       
            $resultant_array["ward_name"] = $ward_result[$ward_counter]['name'];
            $resultant_array["ward_id"] = $ward_result[$ward_counter]['id'];
          
            array_push($final_result_obj, $resultant_array);
            unset($resultant_array);


     }
        

      return $final_result_obj;



        }




        public function superTrend()
        {


                  $sm = new SuperMonthTrend ;

              $return_data = $sm->getSuperMonthAnalysis();  

              return $return_data;         

        }





                public function addWardAdmin(Request $request)
     {
                  

                   $data = $request->all();
                   $data_array = $data["param"]; 

                

                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                   DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_wardAdmin = CASE is_wardAdmin 
                     WHEN 0 then 1 
                     WHEN 1 THEN 1 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );

                        $result_employee = \App\Employee :: select('name','ward_id','email_id')->where('id','=',$data_array[$i])->first();
                        $ward_id = $result_employee['ward_id'];
                       DB:: connection('ahmedabad_connection')->statement("update ward
                        set admin_added=1  WHERE id =$ward_id
                    ;"   );

                   

                    //send mail to every added authority
                              

                       $viewdata= array();
                       $viewdata['role']="WardAdmin";

                       $get_detail = \App\Employee :: select('name','email_id','password')->where('id','=',$data_array[$i])->where('is_AddedAuthority','=',0)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',1)->first();

                       if($get_detail)
                       {

                       $viewdata['name']=$get_detail['name'];

                       $viewdata['email_id']=$get_detail['email_id'];

                       $viewdata['password']=$get_detail['password'];


                      try
                  {
                     $sub="CAS PROCEDURE";
              
             
          
               dispatch(new \App\Jobs\SendEmailForAdd($viewdata));


             // \Mail::send( 'AddTemplate',$viewdata, function($message) use ($viewdata,$sub){
             // $message->to($viewdata['email_id'], $viewdata['name'])->subject
             // ($sub);
             // $message->from("cas276936@gmail.com","Civil Administration System");
             

             // });
             
           
           }
            catch(\Exception $e)
           {
              
              
            
               return $e;

          }
        }
  





            

                   }


                     

                       return "success";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }


        


         
     }

     public function removeWardAdmin(Request $request)
     {

                   $data = $request->all();
                   $data_array = $data["param"]; 
                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                     DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_wardAdmin = CASE is_wardAdmin 
                     WHEN 0 then 0 
                     WHEN 1 THEN 0 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );
                    
                     $result_employee = \App\Employee :: select('name','ward_id','email_id')->where('id','=',$data_array[$i])->first();
                     $ward_id = $result_employee['ward_id'];
                       DB:: connection('ahmedabad_connection')->statement("update ward
                        set admin_added=0  WHERE id =$ward_id
                    ;"   );


                     if($result_employee)
                     {
                     
                        $ward_id = $result_employee['ward_id'];


                     
                   
                          $viewdata=array();
                          $viewdata['email_id']=$result_employee['email_id'];
                          $viewdata['role']  = "WardAdmin";
                          $viewdata['name'] = $result_employee['name'];

                          dispatch(new \App\Jobs\SendEmailForRemove($viewdata));
                      }

                     




                   }

                    return "success";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }





     }



      public function getAddedWardAdmin(Request $request)
    {

              //get all the added employees

          //fetch all the employees corresponding to the particular department

         $employees = \App\Employee :: select('employee.id as admin_id','employee.name as name','email_id','address','contact_no','designation','ward.id as ward_id','ward.name as ward_name')->join('ward','ward.id','=','employee.id')->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',1)->where('is_AddedAuthority','=',0)->get()->toArray();
      
           
         //converting to required format

         $return_data =  new \stdClass();

         $total = sizeof($employees);
         $sort  ="asc";
         $field = "name";
         $page = 1;
         $pages = 1;
         $perpage = 10;


         $meta = array(

                 "page" => $page,
                 "pages" => $pages,
                 "perpage" => $perpage,
                 "total" => $total,
                 "sort"  => $sort,
                 "field"  => $field
         );

         $return_data->meta = $meta;
         $return_data->data = $employees;



         

        return json_encode($return_data);

         
        




    }












     public function getTopWards(Request $request)
     {
             // //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $ward_result = \App\ward :: select('id')->get()->toArray();
                $ward_array = array();
                for($i=0;$i<sizeof($ward_result);$i++)
                {

                    array_push($ward_array,$ward_result[$i]['id']);
                }



                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('ward.name',\DB::raw("count(ward_id) as total_complains"))->join('ward','complains.ward_id','=','ward.id')->whereIn('complains.ward_id',$ward_array)->groupBy('ward.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }



       public function getTopDepartments(Request $request)
     {
             // //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $ward_result = \App\ward :: select('id')->get()->toArray();
                $ward_array = array();
                for($i=0;$i<sizeof($ward_result);$i++)
                {

                    array_push($ward_array,$ward_result[$i]['id']);
                }



                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('department.name',\DB::raw("count(department_id) as total_complains"))->join('department','complains.department_id','=','department.id')->whereIn('complains.ward_id',$ward_array)->groupBy('department.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }

        public function getTopAreas(Request $request)
     {
             // //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $ward_result = \App\ward :: select('id')->get()->toArray();
                $ward_array = array();
                for($i=0;$i<sizeof($ward_result);$i++)
                {

                    array_push($ward_array,$ward_result[$i]['id']);
                }



                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('area.name',\DB::raw("count(area_id) as total_complains"))->join('area','complains.area_id','=','area.id')->whereIn('complains.ward_id',$ward_array)->groupBy('area.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }


     public function getOverall(Request $request)
     {

           
              $result =\DB :: connection('ahmedabad_connection')->table('complains')->select(\DB::raw("count(complains.id) as total_complains"),\DB::raw("COUNT(CASE WHEN complains.status = 'pending' then 1 ELSE NULL END) as pending"),\DB::raw("COUNT(CASE WHEN complains.status = 'complain allocated' then 1 ELSE NULL END) as allocated_complains"),\DB::raw("COUNT(CASE WHEN complains.status = 'solved' then 1 ELSE NULL END) as solved"),\DB::raw("COUNT(CASE WHEN complains.status = 'unsolved' then 1 ELSE NULL END) as unsolved"))->get()->toArray();
              
              $result_solved=\DB::connection('ahmedabad_connection')->table('complain_solved')->select(\DB::raw("COUNT(CASE WHEN user_Satisfaction = 1 then 1 ELSE NULL END) as satisfied"),\DB::raw("COUNT(CASE WHEN user_Satisfaction = 0 then 1 ELSE NULL END) as unsatisfied"))->get()->toArray();
              //dd($result_solved);

              $result[0]->satisfied = $result_solved[0]->satisfied;
              
              $result[0]->unsatisfied=$result_solved[0]->unsatisfied;






                return $result;
     }
     public function getOverallTrend(Request $request)
     {
  $return_data = array();

   for($month=1;$month<=12;$month++)
   {



   $result =\DB :: connection('ahmedabad_connection')->table('complains')->select(\DB::raw("count(complains.id) as total_complains"),\DB::raw("COUNT(CASE WHEN complains.status = 'pending' then 1 ELSE NULL END) as pending"),\DB::raw("COUNT(CASE WHEN complains.status = 'complain allocated' then 1 ELSE NULL END) as allocated_complains"),\DB::raw("COUNT(CASE WHEN complains.status = 'solved' then 1 ELSE NULL END) as solved"),\DB::raw("COUNT(CASE WHEN complains.status = 'unsolved' then 1 ELSE NULL END) as unsolved"))->whereMonth('created_at','=',$month)->whereYear('created_at','=',2019)->get()->toArray();
              
              $result_solved=\DB::connection('ahmedabad_connection')->table('complain_solved')->select(\DB::raw("COUNT(CASE WHEN user_Satisfaction = 1 then 1 ELSE NULL END) as satisfied"),\DB::raw("COUNT(CASE WHEN user_Satisfaction = 0 then 1 ELSE NULL END) as unsatisfied"))->whereMonth('created_at','=',$month)->whereYear('created_at','=',2019)->get()->toArray();
              //dd($result_solved);

              $result[0]->satisfied = $result_solved[0]->satisfied;
              
              $result[0]->unsatisfied=$result_solved[0]->unsatisfied;


              array_push($return_data,$result);



                
    



     }


                 return $return_data;  



     }


     public function getAllWards(Request $request)
     {



            $result = \App\ward :: where('admin_added','=',0)->get()->toArray();
            return $result;



     }

     public function getAllEmployees(Request $request)
     {



            $request_data = $request->all();

             $ward_id =  $request_data['ward_id'];


             $result = \App\Employee :: select('id','name')->where('ward_id','=',$ward_id)->where('is_AddedAuthority','=',0)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->get()->toArray();
              
              return $result;


     }



         public function sendPassword(Request $request)
         {

                         $request_data = $request->all();
              if(isset($request_data['email_id']))
              {
                  $email_id = $request_data['email_id'];
                  

                  $password_result = \App\SuperEmployees :: select('name','password')->where('email_id','=',$email_id)->first();
                  if($password_result)
                  {
                         $user_name = $password_result['name'];
                         $password = $password_result['password'];

                         //send email to $user_name with $email_id
                   $data= array();
                   $data['name']=$user_name;
                   $data['email_id']=$email_id;
                   $data['password']=$password;
                   

                   $sub="forgot password";

                   try
                   {

              \Mail::send( 'usermail',$data, function($message) use ($data,$sub){
             $message->to($data['email_id'], $data['name'])->subject
             ($sub);
             $message->from("cas276936@gmail.com","Civil Administration System");
           
             });

      
             return "success";
           }catch(\Exception $e)
           {
                return -1;
           }


                  }else
                  {
                     return -1;
                  }

              }else
              {

                  return -1;
              }




         }

             



      }

