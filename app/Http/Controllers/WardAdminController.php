<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;






use App\Area;
use App\Department;
use App\EmployeeSessionData;
use App\Repositories\WardDeptMonth;
use App\ward_admin;
use Mail;
use Illuminate\Support\Str;
use App\Employee_Notification;
use DB;
use App\Complains;
use App\Employee;
use App\ComplainSubmittedTo;

class WardAdminController extends Controller
{
    //@author:pravir pal



    public function sendLinkForLogin()
    {
    	//already made a cron for it no need to fill the function //for future use

       
       
       
     // $result = DB::connection('ahmedabad_connection')->table('department')->select('department.id as department','department.ward_id as ward_id')->get()->toArray();
     //   dd($result);
      //$result = DB::connection('ahmedabad_connection')->table('DEPARTMENT')




   }

   public function LogIn(Request $request)
   {
            $data = $request->all();
          

      return view('warddash',compact('data'));
  
   }
   public function LogOut(Request $request)
   {

       $admindata = $request->all();

             //check if you are not logged in
             $admin  = \App\EmployeeSessionData::where('session_id','=',$admindata['session_id'])->where('user_id','=',$admindata['id'])->get()->toArray();
             
              if(!empty($admin[0]) && $admin[0]['deleted_at']==null){
             


                     $destroy = \App\EmployeeSessionData::destroy($admin[0]['id']);
             

                     return "successfully logged out";


              }else
              {
                return "your session is not created yet";
              }

             


   }

   public function getDeptwiseAnalysis(Request $request)
   {
          

           $get_data = $request->all();
         
           $ward_id   = $get_data['ward_id'];


                     
            $departmet_result = \App\department :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();
            
            $department_array=array();
            for($i=0;$i<sizeof($departmet_result);$i++)
            {

               $department_array[$i]=$departmet_result[$i]['id'];
            }
           
            $area_result = \App\area :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();


            $area_array=array();
            for($i=0;$i<sizeof($area_result);$i++)
            {

               $area_array[$i]=$area_result[$i]['id'];
            }
            
              $department_ids = join("','",$department_array);  

         $result =  DB::connection('ahmedabad_connection')->select(" select
        DISTINCT m1.department_id,m1.status, (select count(*)  from complains m2 where m2.status = m1.status and m1.department_id =m2.department_id) as total from
         complains m1 where department_id IN ('$department_ids')");
         
     //  return $result;
         //$result contains values in the format department id,status,total as array of json objects


         //want to find the total satisfied and unsatisfied in each department


         //find all the complain ids of each particular department in ward from department_array

         $result_complain_ids = \App\Complains :: select('department_id','id')->whereIn('department_id',$department_array)->get()->toArray();

          $department_array_length = sizeof($department_array);

          $depart_assoc = array();
           

          for($i=0;$i<$department_array_length;$i++)
          {
             $temp_array= array();

          for($j=0;$j < sizeof($result_complain_ids);$j++)
          {

                  if(isset($result_complain_ids[$j]['id']))
                  {
                     if($result_complain_ids[$j]['department_id'] == $department_array[$i])
                     {
                           $temp_array[$j] = $result_complain_ids[$j]['id'];
                     }
                  }

          }
          $depart_assoc[$department_array[$i]] = $temp_array;
          unset($temp_array);

        }

           $dept_user_array = array();
           
           $counter=0;
          foreach ($depart_assoc as $key => $val)
           {
              $dept_user_obj =  new \stdClass();
                
            $resultun = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',0)->get()->toArray();
            $resultsa = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',1)->get()->toArray(); 
            $len = sizeof($resultun);
            $len_satisfied = sizeof($resultsa);
            $dept_user_obj->department_id=$key;
            $dept_user_obj->user_unsatisfied=$len;
            $dept_user_obj->user_satisfied=$len_satisfied;
           
            $dept_user_array[$counter] = $dept_user_obj;
            array_push($result,$dept_user_obj);
             $counter=$counter+1;
             unset($dept_user_obj);
          
           }

           $final_depart_obj =  array();

           //get the names of the department

           $result_name = \App\department :: whereIn('id',$department_array)->get()->toArray();
           
           //prepare final object
           //$counter=0;
           for($i=0;$i<sizeof($result_name);$i++)
           {

              //make an array to associate each key in object to array of required values
              $resultant_array=array('solved' => 0,'unsolved' => 0,'allocated_complains' => 0,'pending' => 0,'user_satisfaction_count'=> 0,'user_unsatisfaction_count' => 0);

              for($j=0;$j<sizeof($result);$j++)
              {

                 if($result[$j]->department_id == $result_name[$i]['id'])
                 {

                    if(!isset($result[$j]->status) )
                    {

                         $resultant_array["user_satisfaction_count"] = $result[$j]->user_satisfied;
                         $resultant_array["user_unsatisfaction_count"] = $result[$j]->user_unsatisfied; 

                    }else
                    {

                    if($result[$j]->status == "pending")
              
                          $resultant_array['pending'] = $result[$j]->total;
                    elseif ($result[$j]->status == "complain allocated") 
                             $resultant_array['allocated_complains'] = $result[$j]->total;
                    elseif($result[$j]->status == "solved")

                            $resultant_array['solved'] = $result[$j]->total;

                    elseif($result[$j]->status == "unsolved")

                          $resultant_array['unsolved'] = $result[$j]->total;
                    
              
            
             }
              
              
            }
          }//end of inner for loop
//          return $resultant_array;

              //     if(!isset($resultant_array['solved']))
              // {
              //       $resultant_array['solved']==0;
              // }
              //  if(!isset($resultant_array['usolved']))
              // {
              //       $resultant_array['usolved']==0;
              // }
              //  if(!isset($resultant_array['allocated_complains']))
              // {
              //       $resultant_array['allocated_complains']==0;
              // }
              //  if(!isset($resultant_array['pending']))
              // {
              //       $resultant_array['pending']==0;
              // }
             
           

              $resultant_array["total_complains"] = $resultant_array['solved']+
                                                    $resultant_array['unsolved']+
                                                    $resultant_array['pending']+
                                                    $resultant_array['allocated_complains'];
              
              $resultant_array["department_name"]=$result_name[$i]['name'];
              $resultant_array["department_id"] = $result_name[$i]['id'];
             //  return $resultant_array;
             // $final_depart_obj-> = $result_name['id'];
              $final_depart_obj[$i] = $resultant_array;
             

         
           unset($resultant_array);
           
        }//end of outer for loop



   
  return $final_depart_obj;
              // $result=  DB::connection('ahmedabad_connection')
              //     ->table('complains')
              //    ->select('department.id as department_id','department.name as department_name',DB::raw('COUNT(complains.id) as total_complains'))
              //    ->join('department','department.id','=','complains.department_id')
              //    // ->leftjoin('complain_solved', 'complain_solved.complain_id', '=', 'complain_submitted_to.complain_id')
              //    // ->whereIn('complains.department_id','=',$department_array)
              //    // ->whereIn('complains.area_id','=', $area_array)
              //    // ->whereYear('complains.created_at','=',$get_data['year'])
              //    // ->whereMonth('complains.created_at','=',$get_data['month'])
              //     ->groupBy('complains.id')
              //     ->get()
              //    ->toArray();
              //    return $result;
         

















   }




   public function getAreaWiseAnalysis(Request $request)

   {




             $get_data = $request->all();
         
         $ward_id   = $get_data['ward_id'];


                     
            // $departmet_result = \App\department :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();
            
            // $department_array=array();
            // for($i=0;$i<sizeof($departmet_result);$i++)
            // {

            //    $department_array[$i]=$departmet_result[$i]['id'];
            // }
           
            $area_result = \App\area :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();


            $area_array=array();
            for($i=0;$i<sizeof($area_result);$i++)
            {

               $area_array[$i]=$area_result[$i]['id'];
            }
            
              $area_ids = join("','",$area_array);  

         $result =  DB::connection('ahmedabad_connection')->select(" select
        DISTINCT m1.area_id,m1.status, (select count(*)  from complains m2 where m2.status = m1.status and m1.area_id =m2.area_id) as total from
         complains m1 where area_id IN ('$area_ids')");
         
     //  return $result;
         //$result contains values in the format department id,status,total as array of json objects


         //want to find the total satisfied and unsatisfied in each department


         //find all the complain ids of each particular department in ward from department_array

         $result_complain_ids = \App\Complains :: select('area_id','id')->whereIn('area_id',$area_array)->get()->toArray();

          $area_array_length = sizeof($area_array);

          $area_assoc = array();
           

          for($i=0;$i<$area_array_length;$i++)
          {
             $temp_array= array();

          for($j=0;$j < sizeof($result_complain_ids);$j++)
          {

                  if(isset($result_complain_ids[$j]['id']))
                  {
                     if($result_complain_ids[$j]['area_id'] == $area_array[$i])
                     {
                           $temp_array[$j] = $result_complain_ids[$j]['id'];
                     }
                  }

          }
          $area_assoc[$area_array[$i]] = $temp_array;
          unset($temp_array);

        }

           $area_user_array = array();
           
           $counter=0;
          foreach ($area_assoc as $key => $val)
           {
              $area_user_obj =  new \stdClass();
                
            $resultun = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',0)->get()->toArray();
            $resultsa = \App\ComplainSolved :: select('id')->whereIn('complain_id',$val)->where('user_satisfaction','=',1)->get()->toArray(); 
            $len = sizeof($resultun);
            $len_satisfied = sizeof($resultsa);
            $area_user_obj->area_id=$key;
            $area_user_obj->user_unsatisfied=$len;
            $area_user_obj->user_satisfied=$len_satisfied;
           
            $area_user_array[$counter] = $area_user_obj;
            array_push($result,$area_user_obj);
             $counter=$counter+1;
             unset($area_user_obj);
          
           }

           $final_depart_obj =  array();

           //get the names of the department

           $result_name = \App\area :: whereIn('id',$area_array)->get()->toArray();
           
           //prepare final object
           //$counter=0;
           for($i=0;$i<sizeof($result_name);$i++)
           {

              //make an array to associate each key in object to array of required values
              $resultant_array=array('solved' => 0,'unsolved' => 0,'allocated_complains' => 0,'pending' => 0,'user_satisfaction_count'=> 0,'user_unsatisfaction_count' => 0);

              for($j=0;$j<sizeof($result);$j++)
              {

                 if($result[$j]->area_id == $result_name[$i]['id'])
                 {

                    if(!isset($result[$j]->status) )
                    {

                         $resultant_array["user_satisfaction_count"] = $result[$j]->user_satisfied;
                         $resultant_array["user_unsatisfaction_count"] = $result[$j]->user_unsatisfied; 

                    }else
                    {

                    if($result[$j]->status == "pending")
              
                          $resultant_array['pending'] = $result[$j]->total;
                    elseif ($result[$j]->status == "complain allocated") 
                             $resultant_array['allocated_complains'] = $result[$j]->total;
                    elseif($result[$j]->status == "solved")

                            $resultant_array['solved'] = $result[$j]->total;

                    elseif($result[$j]->status == "unsolved")

                          $resultant_array['unsolved'] = $result[$j]->total;
                    
              
            
             }
              
              
            }
          }//end of inner for loop
//          return $resultant_array;

              //     if(!isset($resultant_array['solved']))
              // {
              //       $resultant_array['solved']==0;
              // }
              //  if(!isset($resultant_array['usolved']))
              // {
              //       $resultant_array['usolved']==0;
              // }
              //  if(!isset($resultant_array['allocated_complains']))
              // {
              //       $resultant_array['allocated_complains']==0;
              // }
              //  if(!isset($resultant_array['pending']))
              // {
              //       $resultant_array['pending']==0;
              // }
             
           

              $resultant_array["total_complains"] = $resultant_array['solved']+
                                                    $resultant_array['unsolved']+
                                                    $resultant_array['pending']+
                                                    $resultant_array['allocated_complains'];
              
              $resultant_array["area_name"]=$result_name[$i]['name'];
              $resultant_array["area_id"] = $result_name[$i]['id'];
             //  return $resultant_array;
             // $final_depart_obj-> = $result_name['id'];
              $final_depart_obj[$i] = $resultant_array;
             

         
           unset($resultant_array);
           
        }//end of outer for loop



   
  return $final_depart_obj;
              // $result=  DB::connection('ahmedabad_connection')
              //     ->table('complains')
              //    ->select('department.id as department_id','department.name as department_name',DB::raw('COUNT(complains.id) as total_complains'))
              //    ->join('department','department.id','=','complains.department_id')
              //    // ->leftjoin('complain_solved', 'complain_solved.complain_id', '=', 'complain_submitted_to.complain_id')
              //    // ->whereIn('complains.department_id','=',$department_array)
              //    // ->whereIn('complains.area_id','=', $area_array)
              //    // ->whereYear('complains.created_at','=',$get_data['year'])
              //    // ->whereMonth('complains.created_at','=',$get_data['month'])
              //     ->groupBy('complains.id')
              //     ->get()
              //    ->toArray();
              //    return $result;
         


    }



          public function getDeptMonthTrendAnalysis(Request $request)
          {
                                 //get the admin's department

              $get_data = $request->all();
              
              $wdm = new WardDeptMonth;

              $return_data = $wdm->getWardDeptMonthAnalysis($get_data);  

              return $return_data;         

              
         
        

          }













           public function addDepartmentAdmin(Request $request)
     {
                  

                   $data = $request->all();
                   $data_array = $data["param"]; 

                

                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                   DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_departmentAdmin = CASE is_departmentAdmin 
                     WHEN 0 then 1 
                     WHEN 1 THEN 1 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );
                   
                     $result_employee = \App\Employee :: select('name','department_id','email_id')->where('id','=',$data_array[$i])->first();
                      $department_id = $result_employee['department_id'];
                       DB:: connection('ahmedabad_connection')->statement("update department
                        set admin_added=1  WHERE id =$department_id
                    ;"   );


                    //send mail to every added authority
                              

                       $viewdata= array();
                       $viewdata['role']="DepartmentAdmin";

                       $get_detail = \App\Employee :: select('name','email_id','password')->where('id','=',$data_array[$i])->where('is_AddedAuthority','=',0)->where('is_DepartmentAdmin','=',1)->where('is_wardAdmin','=',0)->first();

                       if($get_detail)
                       {

                       $viewdata['name']=$get_detail['name'];

                       $viewdata['email_id']=$get_detail['email_id'];

                       $viewdata['password']=$get_detail['password'];


                      try
                  {
                     $sub="CAS PROCEDURE";
              
             
          
               dispatch(new \App\Jobs\SendEmailForAdd($viewdata));


             // \Mail::send( 'AddTemplate',$viewdata, function($message) use ($viewdata,$sub){
             // $message->to($viewdata['email_id'], $viewdata['name'])->subject
             // ($sub);
             // $message->from("cas276936@gmail.com","Civil Administration System");
             

             // });
             
           
           }
            catch(\Exception $e)
           {
              
              
            
               return $e;

          }
        }
  





            

                   }


                     

                       return "success";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }


        


         
     }


           public function removeDepartmentAdmin(Request $request)
     {

                   $data = $request->all();
                   $data_array = $data["param"]; 
                   
                    

                   //first of all check if a employee is already added as authority or not
                   try
                   {

                   for($i=0;$i<sizeof($data_array);$i++)
                   {


                     DB:: connection('ahmedabad_connection')->statement("update employee
                     set is_DepartmentAdmin = CASE is_DepartmentAdmin 
                     WHEN 0 then 0 
                     WHEN 1 THEN 0 
                     END
                     WHERE id =$data_array[$i]
                    ;"   );
                    
                     $result_employee = \App\Employee :: select('name','department_id','email_id')->where('id','=',$data_array[$i])->first();
                     $department_id = $result_employee['department_id'];
                       DB:: connection('ahmedabad_connection')->statement("update department
                        set admin_added=0  WHERE id =$department_id
                    ;"   );


                     if($result_employee)
                     {
                     
                        $department_id = $result_employee['department_id'];


                     
                   
                          $viewdata=array();
                          $viewdata['email_id']=$result_employee['email_id'];
                          $viewdata['role']  = "DepartmentAdmin";
                          $viewdata['name'] = $result_employee['name'];

                          dispatch(new \App\Jobs\SendEmailForRemove($viewdata));
                      }

                     




                   }

                    return "1";
                    
               }catch(\Exception $e)
               {
                    return $e;
               }





     }


             public function getAddedDepartmentAdmin(Request $request)
    {

              //get all the added employees

          $get_data = $request->all();
          $ward_id = $get_data['ward_id'];

           //fetch all the employees corresponding to the particular department

      $employees = \App\Employee :: select('employee.id as admin_id','employee.name as name','email_id','address','contact_no','designation','department.id as department_id','department.name as department_name')->join('department','department.id','=','employee.department_id')->where('is_DepartmentAdmin','=',1)->where('is_wardAdmin','=',0)->where('is_AddedAuthority','=',0)->where('employee.ward_id','=',$ward_id)->get()->toArray();
  
           
         //converting to required format

         $return_data =  new \stdClass();

         $total = sizeof($employees);
         $sort  ="asc";
         $field = "name";
         $page = 1;
         $pages = 1;
         $perpage = 10;


         $meta = array(

                 "page" => $page,
                 "pages" => $pages,
                 "perpage" => $perpage,
                 "total" => $total,
                 "sort"  => $sort,
                 "field"  => $field
         );

         $return_data->meta = $meta;
         $return_data->data = $employees;



         

        return json_encode($return_data);

         
        




    }





















          public function getTopDepartments(Request $request)
     {
                $request_data = $request->all();
                $ward_id = $request_data['ward_id'];
           //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $department_result = \App\Department :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();
                $department_array = array();
                for($i=0;$i<sizeof($department_result);$i++)
                {

                    array_push($department_array,$department_result[$i]['id']);
                }

                 

                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('department.name',\DB::raw("count(department_id) as total_complains"))->join('department','complains.department_id','=','department.id')->whereIn('department_id',$department_array)->groupBy('department.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }

               public function getTopAreas(Request $request)
     {
                $request_data = $request->all();
                $ward_id = $request_data['ward_id'];
           //      $result = DB::connection('ahmedabad_connection')->statement("SELECT area.name,COUNT(area_id) AS total_complains 
           //  FROM     complains join area on complains.area_id=area.id where complains.department_id=$department_id
           //  GROUP BY area_id
           // ORDER BY total_complains DESC
           // LIMIT    5");

                $area_result = \App\area :: select('id')->where('ward_id','=',$ward_id)->get()->toArray();
                $area_array = array();
                for($i=0;$i<sizeof($area_result);$i++)
                {

                    array_push($area_array,$area_result[$i]['id']);
                }

                 

                $result = \DB :: connection('ahmedabad_connection')->table('complains')->select('area.name',\DB::raw("count(area_id) as total_complains"))->join('area','complains.area_id','=','area.id')->whereIn('area_id',$area_array)->groupBy('area.name')->orderBy('total_complains','DESC')->limit(5)->get();
                 return $result;


     }

 public function getAllDepartments(Request $request)
     {
                 $request_data  = $request->all();
                  $ward_id = $request_data['ward_id'];


            $result = \App\department :: where('admin_added','=',0)->where('ward_id','=',$ward_id)->distinct('name')->get()->toArray();
            return $result;



     }
    
        public function getAllEmployees(Request $request)
     {



            $request_data = $request->all();

             $ward_id =  $request_data['ward_id'];
             $department_id = $request_data['department_id'];


             $result = \App\Employee :: select('id','name')->where('ward_id','=',$ward_id)->where('department_id','=',$department_id)->where('is_AddedAuthority','=',0)->where('is_DepartmentAdmin','=',0)->where('is_wardAdmin','=',0)->get()->toArray();
              
              return $result;


     }


   }






