<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\UserSessionData;
use App\Repositories\UserRepository;
use App\Complains;
use DB;
use PDF;
class UsersController extends Controller
{
    //

    public function register(Request $request)
    {          

               $userdata = $request->all();

               //check if user exist or not
               $user_check = \App\Users :: where('email','=',$userdata['email'])->first();
               if($user_check)
               {
                    return "User already exist";
               }else
               {

               $username = $userdata['name'];
               $name = explode(" ",$username);
               $len = sizeof($name);
               for($i=0;$i<$len;$i++)

               $lastname = $name[1];
               $firstname =$name[0];
               unset($userdata['name']);
               $userdata['first_name']=$firstname;
               $userdata['last_name']=$lastname;
               $result =  \App\Users :: create($userdata);
               if($result)
               {
                  return "success";
               }else
               {
                 return "error";
               }
           
          }


    }

    public function login(Request $request)
    {


              return $request->all();
                     


    }
    public function logout(Request $request)
    {
        $logoutdata = $request->all();

        $result = \App\UserSessionData :: select('id')->where('user_id','=',$logoutdata['user_id'])->where('session_id','=',$logoutdata['session_id'])->first();
        if($result)
        {
           $deletesession = \App\UserSessionData :: destroy($result['id']);
           if($deletesession)
           {
              return "success";

           }else
           {
              return "error";

           }

        }else
        {
           return "already logged out";
        }



    }
    public function registerComplain(Request $request)
    {

     
         try
         {

          
          $photo_link="";  
          $requestData = $request->all();  
          $complainarray=array();
          $complainarray['user_id']=intval($requestData['user_id']);
          $complainarray['description']=$requestData['description'];
          $complainarray['type']=$requestData['type'];
          //upload image
          $upload = new UserRepository;

         $result_photo = $upload->uploadImage("registerComplain");
         if($result_photo == "failure")
         {
             return -1;
           
         }else
         {
            
            //$photo_link = $result_image_link;
            $complainarray['photo']="http://cas.mindhackers.org/ahmedabad_complain_images/". $_REQUEST['file_name'];

         }

         if(isset($_FILES["videoToUpload"]["name"]))
         {
              
               $result_video_link = $upload->uploadVideo();
               if($result_video_link == "error")
               {
                    return "error";
               }else
               {

                   $complainarray['video']="http://cas.mindhackers.org/ahmedabad_complain_videos/". $_FILES['videoToUpload']['name'];
                   



               }
         }

         $complainarray['status']="pending";
         $complainarray['area_id']=intval($requestData['area_id']);

         //fetch the corresponding department_id from index requested correspondence to area_ward

         $ward =  \App\area :: select('ward_id')->where('id','=',intval($requestData['area_id']))->first();
         $ward_id = $ward['ward_id'];

         $departments = \App\department :: select('id')->where('ward_id','=',$ward_id)->get();
         
         $count=1;
         
         $department_id=0;
         foreach ($departments as $department) {
               
               if($count == intval($requestData['department_id']))
               {
                           

                           $department_id = $department->id;
                       

               }

               $count=$count+1;

            }

         
         $complainarray['department_id']=$department_id;

         $complainarray['latitude']=floatval($requestData['latitude']);
         $complainarray['longitude']=floatval($requestData['longitude']);
         $complainarray['ward_id']=$ward_id;
         $complainarray['allow']=$requestData['allow'];
            

         $complain_insert = \App\Complains :: create($complainarray);

         if($complain_insert)
         {
             $resultdata=[];
             
             $resultdata["message"] = "success";
             $resultdata["complain_id"] = $complain_insert->id;
             $complain_id=$complain_insert->id;

             //send the complain to queue for complain allocation

              // $job = (new \App\Jobs\AllocateComplain())->delay(60);

              // $this->dispatch($job);
             //dispatch();

                      //get token with respect to user

                $token_result = \App\Users :: select('instance_id','first_name')->where('id','=',intval($requestData['user_id']))->first();
                $token =$token_result['instance_id'];

                 $mess_user =  \App\Users :: select('first_name','email')->where('id','=',intval($requestData['user_id']))->first();
                 $name = $mess_user['first_name'];
                $message = "Hello $name !,your complain is succesfully registered with complain_id=$complain_id..you can check the status of complain in CAS Android App";


                         //send push notification and email 
                try{


                 //$this->sendNotificationToUser(intval($requestData['user_id']),$token,$message);
                  $viewdata1 = array();
                  $viewdata1['token']= $token;
                     
                  $viewdata1['text']=$message;

                      

                 dispatch(new \App\Jobs\SendNotiForRegister($viewdata1));

                  $insert_notification =  \App\User_Notification :: create(['content' => $message,'type'=>'system generated','is_read' => 0 ,'from_employee_id'=>0,'to_user_id' => intval($requestData['user_id'])]);

                    }catch(\Exception $e)
                    {

                    } 
                    try{

                      $viewdata = array();
                      $viewdata['email_id']= $mess_user['email'];
                     
                      $viewdata['text']=$message;

                             


                            dispatch(new \App\Jobs\SendEmailForRegister($viewdata));

                           //$this->sendEmail($message,$mess_user['email'],"Complain Registeration");
                    } catch(\Exception $e)
                    {


                    }   






            
             return  json_encode($resultdata);
             
         }
         else
         {
            return "error";
         }
  
    }catch(\Exception $e)
    {


           return "exception=".$e;
    }




       

         

    }

  //author : pravir pal'
  // input : request object containing complain id
  //output : returns status of complain if exists else error 
   

    public function getStatus(Request $request)
    {

            try
            {
         $getdata = $request->all();
         

         $status = \App\Complains :: select('status')->where('id','=',$getdata['complain_id'])->first();

         if($status)
         {
            //
              if($status['status'] == "solved")
              {
                  //get the image link for solution proof

                   $getimage = \App\ComplainSolved :: select('proof_doc')->where('complain_id','=',$getdata['complain_id'])->first();
                   $proof = $getimage['proof_doc'];
                   $status['proof'] = $proof;
                   return $status;




              }else
              {
              
              $status['proof'] = "not found";
             return $status;
             
              }
         }
         else
         {
         return -1;
         } 
       }catch(\Exception $e)
       {
           return $e;
       }

    }


    //author : pravir pal
    //input  : request object containing profile updation details
    //output : returns success or error for successful updation or deletion



    public function updateProfile(Request $request)
    {



                $data = $request->all();

                
                //make an model to update for user

                $user_update = \App\Users :: find($data['user_id']);

                if(isset($_REQUEST['fileToUpload']))
                {
                  
                   $upload = new UserRepository;

                  
                   $photo_result = $upload->uploadImage("updateProfile");

                   if($photo_result == "failure")
                   {

                        return -1;
                   }else
                  {
                        
                    $user_update->profile_image = "http://cas.mindhackers.org/profile_images/".$_REQUEST['file_name'];

                  }


                }else
                {
                  $user_update->profile_image = null;


                }



                //check other variables are set or not

              
              $user_update->first_name = isset($data['first_name']) ? $data['first_name'] : null ; 
              $user_update->last_name = isset($data['last_name']) ? $data['last_name'] : null ; 
              $user_update->gender = isset($data['gender']) ? $data['gender'] : null ; 
              $user_update->birth_date = isset($data['birth_date']) ? $data['birth_date'] : null ; 
              $user_update->phone = isset($data['phone']) ? $data['phone'] : null ; 
              $user_update->address = isset($data['address']) ? $data['address'] : null ;
              $user_update->city = isset($data['city']) ? $data['city'] : null ;
              $user_update->state = isset($data['state']) ? $data['state'] : null ; 
              
                 

                 //check if the variables are succesfully updated or not



              if($user_update->save())
              {



                  $result = \App\Users :: where('id','=',$data['user_id'])->first();
                  return $result;
              
              }else
              {


                    return -1;
              }










    } 



    //notification from user to authority

    public function sendNotificationToAuthority(Request $request)
    {



                   $notify_data = $request->all();

                   $from_user_id = $notify_data['from_user_id'];

                   $to_au_id = $notify_data['from_user_id'];










    }

   //

    public function giveAllCertificates(Request $request)
    {


          
          $request_data =  $request->all();
          
          $user_id = $request_data['user_id'];

          $result = \App\GeneratedCertificate :: select('certificate_link','complain_id')->where('user_id','=',$user_id)->get()->toArray();
         
        
     

         if($result)
         {

              $return_data =  new \stdClass();

             $return_data->user_certificates = $result;
        
             return json_encode($return_data);
             
         }else
         {

             return -1;
         }
           
                     


   }


   public function giveAllRecentNotifications(Request $request)
   {
             $request_data =  $request->all();
          
             $user_id = $request_data['user_id'];

            $result =  \App\User_Notification ::  select('content','created_at')->where('to_user_id','=',$user_id)->orderBy('created_at','DESC')->get()->toArray();

            if($result)
            {
                  $return_data =  new \stdClass();

             $return_data->user_notifications = $result;
        
             return json_encode($return_data);
             

            }else
            {
                return -1;
            }

         


   }
  


//   
    public function forgotPassword(Request $request)
    {

              $request_data = $request->all();
              if(isset($request_data['email_id']))
              {
                  $email_id = $request_data['email_id'];
                  

                  $password_result = \App\Users :: select('first_name','password')->where('email','=',$email_id)->first();
                  if($password_result)
                  {
                         $user_name = $password_result['first_name'];
                         $password = $password_result['password'];

                         //send email to $user_name with $email_id
                   $data= array();
                   $data['name']=$user_name;
                   $data['email_id']=$email_id;
                   $data['password']=$password;

                   $sub="forgot password";

                   try
                   {

              \Mail::send( 'usermail',$data, function($message) use ($data,$sub){
             $message->to($data['email_id'], $data['name'])->subject
             ($sub);
             $message->from("cas276936@gmail.com","Civil Administration System");
           
             });

      
             return "success";
           }catch(\Exception $e)
           {
                return -1;
           }


                  }else
                  {
                     return -1;
                  }

              }else
              {

                  return -1;
              }


    }

  
    //change password 


    public function changePassword(Request $request)
    {


         $request_data= $request->all();


           if(isset($request_data['current_password'])&& isset($request_data['new_password']))
           {


                 $password_result = \App\Users :: select('id')->where('password','=',$request_data['current_password'])->where('id','=',$request_data['user_id'])->first();
                 if($password_result)
                 {

                     $us = \App\Users :: find($password_result['id']);
                     $us->password=$request_data['new_password'];
                     if($us->save())
                     {
                        try{
                        
                       $user_result = \App\Users :: select('first_name','email')->where('id','=',$request_data['user_id'])->first();

                        $name = $user_result['first_name'];
                         $content="Hey $name !! your password has been succesfully reset";
                         $type="system generated";
                         $is_read=0;
                         $to_user_id = $request_data['user_id'];
                         $from_employee_id = 0;
                         $noti_array = array('from_employee_id'=>0,'to_user_id'=>$to_user_id,'content'=>$content,'type'=>$type,'is_read'=>$is_read);

                         $insert_noti = \App\User_Notification :: create($noti_array);
                         
                           }
                           catch(\Exception $e)
                           {
                            //return "catch";


                           }
                         
                                //get token with respect to user

                $token_result = \App\Users :: select('instance_id','first_name')->where('id','=',$request_data['user_id'])->first();
                $token =$token_result['instance_id'];

                 $mess_user =  \App\Users :: select('first_name','email')->where('id','=',$request_data['user_id'])->first();
                 $name = $mess_user['first_name'];
                $message = "Hello $name !,you have succesfully reseted your password";


                         //send push notification and email 
                try{

                 $this->sendNotificationToUser($request_data['user_id'],$token,$message);
                    }catch(\Exception $e)
                    {

                    } 
                    try{
                           $this->sendEmail($message,$mess_user['email'],"Password Reset");
                    } catch(\Exception $e)
                    {


                    }   

                         return 1;

                     }else
                     {
                        return -1;
                     }

                 }else
                 {
                    return -1;
                 }


           }else
           {
              return -1;
           }
    }




     public function checkSatisfaction(Request $request)
     {

            $request_data = $request->all();
            try
            {

            $user_satisfaction = $request_data['user_satisfaction'];
            $complain_id = $request_data['complain_id'];
             $result = \App\ComplainSolved :: select('id')->where('complain_id','=',$complain_id)->first();
             if($result)
             {
                DB::connection('ahmedabad_connection')->statement("UPDATE complain_solved SET user_satisfaction=$user_satisfaction where complain_id=$complain_id");
            return 1;
             }else
             {
              return -1;
             }
          }catch(\Exception $e)
          {
              return -1;
          }


     }


   public function getAllComplains(Request $request)
   {

        $request_data = $request->all();
        $user_id = $request_data['user_id'];
        $result_complain = \App\Complains :: select('id as complain_id','description','photo','video','type','created_at','status')->where('user_id','=',$request_data['user_id'])->orderBy('created_at','desc')->get()->toArray();

        $return_data =  new \stdClass();

        $return_data->complain_data = $result_complain;
        
        return json_encode($return_data);
     




   }


        public function sendNotificationToUser($user_id,$token,$message)
     {

                 $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
       // $token='fmt3ssjef7s:APA91bGuXerFVN4CkvAyvfQBvuMVcJrdmXy2rKjjeU36JvVmoyc0g2YR-Tpv8ByH84dWUpmcPTyQL68iqkZpis1Zta6hV6PiHwK2Z0KcjrMbaCKwR1gdegzNp0Oqyouoqd_LhPADaWRH';
    

    $notification = [
        'body' => $message,
        'sound' => false
    ];
    
    $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

    $fcmNotification = [
        //'registration_ids' => $tokenList, //multple token array
        'to'        => $token, //single token
        'notification' => $notification,
        'data' => $extraNotificationData
    ];

    $headers = [
        'Authorization: key=AIzaSyC4XQO8kHhHcE3UfE1pgrEuVMHI_sCE2kY',
        'Content-Type: application/json'
    ];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = json_decode(curl_exec($ch));
    curl_close($ch);
    
   
   $result->user_message = $message;


    
     return json_encode($result);
 



     }


    public function sendEmail($message,$email,$subject)
    {

              $viewdata=array();
              $viewdata['content']=$message;
              $viewdata['email_id']=$email;
              $sub=$subject;
              \Mail::send( 'usertemplatemail',$viewdata, function($message) use ($viewdata,$sub){
              $message->to($viewdata['email_id'])->subject
              ($sub);
              $message->from("cas276936@gmail.com","Civil Administration System");
             

              });
 


    }








}
