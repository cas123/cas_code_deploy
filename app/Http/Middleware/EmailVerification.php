<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\HelperRepositoryInterface;

class EmailVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed

     */
    private $helper;
    public function __construct(HelperRepositoryInterface $helper)
    {
        $this->helper=$helper;

    }
    public function handle($request, Closure $next)
    {
         $data = $request->all();

         $result = $this->helper->sendMail($data);

         if($result == "success")
         {
              
              return $next($request);  
         }else
         {
              
         return response('email is either already registered or such email does not exist.');

         }
         

         
    }
}
