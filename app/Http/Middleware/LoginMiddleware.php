<?php

namespace App\Http\Middleware;

use Closure;
use App\SuperEmployees;
use App\SessionData;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

                  $logindata = $request->all();

                  $result = \App\SuperEmployees :: where('name','=',$logindata['name'])->where('password','=',$logindata['password'])->get()->toArray();
                  if($result)
                  {

                      $emailverify = $result[0]['email_verified_at'];


                      
                      if($emailverify == null)
                      {

                          return response("please activate your account with the link given in email");

                      }else
                      {
                                // check if already a user is logged in or not if yes than no need to create session just show the logged in dashboard

                            $resultsession = \App\SessionData :: where('user_id','=',$result[0]['id'])->where('deleted_at','=',null)->get()->toArray();

                            

                            if(!empty($resultsession))
                            {  

                                        return $next($request);

                            } else
                            {
                                 $session_id = md5(microtime().$_SERVER['REMOTE_ADDR']);
                                 $user_id = $result[0]['id'];
                                 $sessionarray= array();
                                 $sessionarray['user_id'] = $user_id;
                                 $sessionarray['session_id'] = $session_id;
                                 $insertsession = \App\SessionData :: create($sessionarray); 
                                 $request->request->add($sessionarray);
                                 return $next($request);
                           
                               

                            }

                           
                            
                      }
                    

                  }else
                  {

                      return response("username or password is incorrect");
                      }

        
    }
}
