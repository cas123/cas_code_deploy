<?php

namespace App\Http\Middleware;

use Closure;
use App\Users;
use App\UserSessionData;
class UsersLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

         $logindata = $request->all();
         $resultdata = \App\Users :: where('email','=',$logindata['email'])->where('password','=',$logindata['password'])->first();
         if(!isset($logindata['instance_id']))
         {
                  return "instance_id required";
         }
         $instance_id = $logindata['instance_id'];
        if($resultdata)
        {
            //create a session for the user

            //insert new insert id

                    $us = \App\Users :: find($resultdata['id']);
                    $us->instance_id = $instance_id;
                    if(!$us->save())
                    {
                         return "unable_to_register_instance";

                    }
                    $resultdata['instance_id'] = $instance_id;

                    $resultsession = \App\UserSessionData :: where('user_id','=',$resultdata['id'])->where('deleted_at','=',null)->get()->toArray();

                            

                            if(!empty($resultsession))
                            {  

                                 $s_id = $resultsession[0]['session_id'];
                                  $resultdata['session_id']=$s_id;

                                 $request->request->add($resultdata->toArray());
                                 
                                 return $next($request);
                        
                            

                            }else
                            {

 

                                 $session_id = md5(microtime().$_SERVER['REMOTE_ADDR']);
                                 $user_id = $resultdata['id'];
                                 $sessionarray= array();
                                 $sessionarray['user_id'] = $user_id;
                                 $sessionarray['session_id'] = $session_id;
                                 $insertsession = \App\UserSessionData :: create($sessionarray); 
                                 $resultdata['session_id']=$session_id;
                                 $request->request->add($resultdata->toArray());
                                 
                                 return $next($request);
                             }
                           


           


        }else
        {
            return response("-1");
        }
        

         
    }
}
