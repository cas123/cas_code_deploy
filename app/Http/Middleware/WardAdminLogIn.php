<?php

namespace App\Http\Middleware;

use Closure;
use \App\Employee;

use App\EmployeeSessionData;

class WardAdminLogIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          $logindata = $request->all();
         $resultdata = \App\Employee :: where('email_id','=',$logindata['email_id'])->where('password','=',$logindata['password'])->where('is_wardAdmin','=',1)->where('is_departmentAdmin','=',0)->where('is_AddedAuthority','=',0)->first();
        if($resultdata)
        {
            //create a session for the user


                 $resultsession = \App\EmployeeSessionData :: where('user_id','=',$resultdata['id'])->where('deleted_at','=',null)->get()->toArray();

                            

                            if(!empty($resultsession))
                            {  
                                          $s_id = $resultsession[0]['session_id'];
                                  $resultdata['session_id']=$s_id;

                                 $request->request->add($resultdata->toArray());
                                 
                                        return $next($request);

                            } else
                            {

                                 $session_id = md5(microtime().$_SERVER['REMOTE_ADDR']);
                                 $user_id = $resultdata['id'];
                                 $sessionarray= array();
                                 $sessionarray['user_id'] = $user_id;
                                 $sessionarray['session_id'] = $session_id;
                                 $insertsession = \App\EmployeeSessionData :: create($sessionarray); 
                                 $resultdata['session_id']=$session_id;
                                 $request->request->add($resultdata->toArray());
                                 return $next($request);
                           }


           


        }else
        {
            return response("invalid email id or password");
        
    }
}
}