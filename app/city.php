<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class city extends Model
{
    //
          protected $connection = 'ahmedabad_connection';
     protected $tables = 'city';
    protected $fillable=['name','state_id','country_id'];
    protected $guarded=['id'];
}
