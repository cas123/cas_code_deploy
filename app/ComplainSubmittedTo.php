<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplainSubmittedTo extends Model
{
    //


         
       protected $connection = 'ahmedabad_connection';
      protected $table = 'complain_submitted_to';
      protected $fillable = ['complain_id','employee_id','from_user_id'];
      protected $guarded = ['id','created_at','updated_at'];

}

