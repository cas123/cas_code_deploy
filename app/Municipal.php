<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipal extends Model
{
    //
    protected $table='municipal';
    protected $fillable = ['District','Taluka','Municipal'];
    protected $guarded=['id'];
}
