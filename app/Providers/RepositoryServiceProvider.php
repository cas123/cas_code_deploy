<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->bootRepositories();
    }
    public function bootRepositories()
    {
        $this->app->bind('App\Repositories\SuperAdminRepository\SuperAdminRegisterInterface','App\Repositories\SuperAdminRepository\SuperAdminRegister');
        $this->app->bind('App\Repositories\HelperRepositoryInterface','App\Repositories\HelperRepository');
        $this->app->bind('App\Repositories\HelperRepository\UserRepository');
         $this->app->bind('App\Repositories\WardDeptMonth');
          $this->app->bind('App\Repositories\SuperMonthTrend');




    }
}
