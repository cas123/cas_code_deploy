<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SuperEmployees extends Model 
{
    //
  
    protected $tables = 'super_employees';
    protected $connection = 'cas_main_connection';
    protected $fillable=['name','email_verified_at','email_id','password','prefer_name','address','birth_date','work_phone','job_title'];
    protected $guarded=['id'];
    public $timestamps=false;
    public function codeVerify()
    {
    	return $this->hasOne('App\CodeVerify','email_id','email_id');
    }

}
