<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class state extends Model
{
    //
     protected $connection = 'ahmedabad_connection';
     protected $tables = 'state';
    protected $fillable=['name','country_id'];
    protected $guarded=['id'];
}
