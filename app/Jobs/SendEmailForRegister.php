<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
//use App\Mail\SendEmailForAddView as SendEmailMail;
use Mail;

class SendEmailForRegister implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
 public $details=array();
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($details)
    {
        //
         $this->details['email_id']=$details['email_id'];
        $this->details['text']=$details['text'];
        

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
          $viewdata=array();
              $viewdata['email_id']=$this->details['email_id'];
              //$viewdata['name']=$this->details['name'];
              //$viewdata['password']=$this->details['password'];
              $viewdata['text']=$this->details['text'];
              
            
               $sub="Complain Registered Successfully";
              \Mail::send( 'RegisterTemplate',$viewdata, function($message) use ($viewdata,$sub){
              $message->to($viewdata['email_id'])->subject
              ($sub);
              $message->from("cas276936@gmail.com","Civil Administration System");
             

              });
 



    }
}
