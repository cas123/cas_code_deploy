<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
//use App\Mail\SendEmailForAddView as SendEmailMail;
use Mail;


class SendEmailForRemove implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $details=array();

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        //
        $this->details['email_id']=$details['email_id'];
        $this->details['role']=$details['role'];
        $this->details['name']=$details['name'];


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
           
         //$email = new \App\Mail\SendEmailForAddView($this->details);
   
              $viewdata=array();
              $viewdata['email_id']=$this->details['email_id'];
              $viewdata['role']=$this->details['role'];
              $viewdata['name']=$this->details['name'];
              
            
               $sub="CAS PROCEDURE";
              \Mail::send( 'RemoveTemplate',$viewdata, function($message) use ($viewdata,$sub){
              $message->to($viewdata['email_id'])->subject
              ($sub);
              $message->from("cas276936@gmail.com","Civil Administration System");
             

              });
 


    }

}
