<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendNotiForRegister implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
     public $details=array();
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($details)
    {
        //
          $this->details['token']=$details['token'];
        $this->details['text']=$details['text'];
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
                   $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                    $viewdata=array();
              $viewdata['text']=$this->details['text'];
              //$viewdata['name']=$this->details['name'];
              //$viewdata['password']=$this->details['password'];
              $viewdata['token']=$this->details['token'];
              
          
       

    $notification = [
        'body' => $viewdata['text'],
        'sound' => false
    ];
    
    $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

    $fcmNotification = [
        //'registration_ids' => $tokenList, //multple token array
        'to'        => $viewdata['token'], //single token
        'notification' => $notification,
        'data' => $extraNotificationData
    ];

    $headers = [
        'Authorization: key=AIzaSyC4XQO8kHhHcE3UfE1pgrEuVMHI_sCE2kY',
        'Content-Type: application/json'
    ];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = json_decode(curl_exec($ch));
    curl_close($ch);
    
   
   $result->user_message = $message;


    
     return json_encode($result);
 


    }
}
