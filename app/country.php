<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    //
     protected $connection = 'ahmedabad_connection';
     protected $tables = 'country';
    protected $fillable=['name'];
    protected $guarded=['id'];
}
