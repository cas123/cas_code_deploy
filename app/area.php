<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class area extends Model
{
    //
         protected $connection = 'ahmedabad_connection';
     protected $table = 'area';
    protected $fillable=['name','ward_id','city_id','state_id','country_id'];
    protected $guarded=['id'];
}
