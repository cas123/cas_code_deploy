<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
      protected $connection = 'ahmedabad_connection';
      protected $table = 'employee';
      protected $fillable = ['password','name','email_id','address','ward_id','contact_no','department_id','designation','is_wardAdmin','is_departmentAdmin','is_AddedAuthority','notification_flag','notification_count'];
      protected $guarded = ['id','created_at','updated_at'];

}
