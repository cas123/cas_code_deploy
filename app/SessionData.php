<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SessionData extends Model
{
    //
    use SoftDeletes;
    protected $table = 'session_data';
    protected $fillable = ['user_id','session_id'];
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

}
