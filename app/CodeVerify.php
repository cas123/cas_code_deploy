<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeVerify extends Model
{
    //
    protected $table='code_verify';
    protected $fillable = ['email_id','secretkey'];
    protected $guarded=['id','created_at','updated_at'];
    public function superEmployee()
    {
    	return $this->belongsTo('App\SuperEmployees','email_id','email_id');
    }
}
