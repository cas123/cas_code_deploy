<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complains extends Model
{
    //
      protected $connection = 'ahmedabad_connection';
      protected $table = 'complains';
      protected $fillable = ['user_id','description','photo','video','status','type','area_id','department_id','ward_id','latitude','longitude','allow'];
      protected $guarded = ['id','created_at','updated_at'];

}
