<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
      protected $connection = 'ahmedabad_connection';
      protected $table = 'users';
      protected $fillable = ['first_name','last_name','email','password','gender','birth_date','notification_flag','profile_image','device_type','latitude','longitude','country','state','first_login','last_active','last_login','city','address','notification_count'];
      protected $guarded = ['id','created_at','updated_at'];


}
