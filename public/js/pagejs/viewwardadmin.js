//== Class definition

var DatatableRecordSelectionDemo = function() {
	//== Private functions

	var options = {
		// datasource definition
		data: {
			type: 'remote',
			source: {
				read: {
					url: 'http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/addedwardadmin',
					method: 'GET',
				},
			},
			pageSize: 10,
			serverPaging: true,
			//serverFiltering: true,
			//serverSorting: true,
		},

		// layout definition
		layout: {
			theme: 'default', // datatable theme
			class: '', // custom wrapper class
			scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
			height: 550, // datatable's body's fixed height
			footer: false // display/hide footer
			
		},
		

		// column sorting
		sortable: true,

		pagination: true,

		// columns definition
		columns: [
			{
				field: 'admin_id',
				title: '#',
				sortable: false,
				width: 40,
				textAlign: 'center',
				selector: {class: 'm-checkbox--solid m-checkbox--brand'},
			},  {
				field: 'name',
				title: 'Name',
				width: 70,
				
			},
			{
				field: 'email_id',
				title: 'EmailID',
				width: 150,
			},  
			{
				field: 'ward_name',
				title: 'Ward',
			}, 
			
			{
				field: 'address',
				title: 'Address',
				width: 100,
			}, {
				field: 'contact_no',
				title: 'Contact',
			},
			{
				field: 'designation',
				title: 'Designation',
			},   

			],
	};

	// basic demo
	var localSelectorDemo = function() {

		options.search = {
			input: $('#generalSearch'),
		};

		var datatable = $('#local_record_selection').mDatatable(options);

		

		$('#m_form_status,#m_form_type').selectpicker();

		datatable.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function(e) {
			var checkedNodes = datatable.rows('.m-datatable__row--active').nodes();
			var count = checkedNodes.length;
			$('#m_datatable_selected_number').html(count);
			if (count > 0) {
				$('#m_datatable_group_action_form').collapse('show');
			} else {
				$('#m_datatable_group_action_form').collapse('hide');
			}
		});

		$('#m_modal_fetch_id').on('show.bs.modal', function(e) {
			var ids = datatable.rows('.m-datatable__row--active').
				nodes().
				find('.m-checkbox--single > [type="checkbox"]').
				map(function(i, chk) {
					return $(chk).val();
				});
			var c = document.createDocumentFragment();
			for (var i = 0; i < ids.length; i++) {
				var li = document.createElement('li');
				li.setAttribute('data-id', ids[i]);
				li.innerHTML = 'Selected record ID: ' + ids[i];
				c.appendChild(li);
			}
			$(e.target).find('.m_datatable_selected_ids').append(c);
		}).on('hide.bs.modal', function(e) {
			$(e.target).find('.m_datatable_selected_ids').empty();
		});

		$('#m_datatable_delete_all').on("click",function(e){
		var ids = datatable.rows('.m-datatable__row--active').
				nodes().
				find('.m-checkbox--single > [type="checkbox"]').
				map(function(i, chk) {
					return $(chk).val();
				});
				//alert(ids.length);
				
				

				for(var i=0;i<ids.length;i++)
				{
					shah(ids,i);
				}
				//delete function
				function shah(ids,i){
					$.ajax({
					type: "GET",
					url:"http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/deletewardadmin?param[]="+ids[i],
					dataType: "json",
					success:function(data){
						if(data.success=="success"){
							setTimeout(function(){// wait for 5 secs(2)
           					location.reload(); // then reload the page.(3)
      						}, 2000);
						}
						alert("deleted successfully");
					}
				});
				}
			});

	};

	

	return {
		// public functions
		init: function(abc) {
			localSelectorDemo();
			//serverSelectorDemo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableRecordSelectionDemo.init();
});