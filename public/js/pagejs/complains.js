//== Class definition

var DatatableJsonRemoteDemo = function () {
	//== Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.m_datatable').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/authority/getcomplains?employee_id='+7,
				pageSize: 10,
				//serverpaging: true,
				 serverFiltering: true,
        		//serverSorting: true,
				method: "POST",
			},

			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},
			rows: {
        callback: function(row, data, index) {
        },
      },


			// columns definition
			columns: [ {
				field: "complain_id",
				title: "ID"
			}, {
				field: "department_name",
				title: "Department",
				
			},
			{
				field: "user_first_name",
				title: "Name",
				
			}, {
				field: "user_email",
				title: "Email",
				width: 110
			}, {
				field: "user_contact",
				title: "Contact",
				responsive: {visible: 'lg'},
			}, {
				field: "user_profile_image",
				title: "Image",
				responsive: {visible: 'lg'},
				template: function(data) {
            return '<a class="m-link" href="'+data.user_profile_image+'" target="_blank">'+data.user_profile_image+'</a>'
          },

        
			}, 
			{
				field: "status",
				title: "Status",
				// callback function support for column rendering
				template: function (row) {
					var status = {
						
						1: {'title': 'Allocated', 'class': ' m-badge--primary'},
						3: {'title': 'Solved', 'class': ' m-badge--success'},
						
						2: {'title': 'Unsolved', 'class': ' m-badge--danger'}
		
					};
					return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
				}
			},


			

				 {
				field: "latitude",
				width: 110,
				title: "Map",
				sortable: false,
				overflow: 'visible',
				template: function (row, index, datatable,data) {
					var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
					return '\
						<a href="https://www.google.com/maps/search/?api=1&query='+row.latitude+','+row.logitude+'" target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Track Complain">\
							<i class="la la-edit"></i>\
						</a>\
						<a href="http://cas.mindhackers.org/cas_code_deploy/public/api/solvedproof?complain_id='+row.complain_id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Proof">\
							<i class="la la-download"></i>\
						</a>\
						';
				}
			}]
		});

		var query = datatable.getDataSourceQuery();

		$('#m_form_status').on('change', function () {
			datatable.search($(this).val(), 'status');
		}).val(typeof query.Status !== 'undefined' ? query.Status : '');

		$('#m_form_type').on('change', function () {
			datatable.search($(this).val(), 'Type');
		}).val(typeof query.Type !== 'undefined' ? query.Type : '');

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		// public functions
		init: function (abc) {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	DatatableJsonRemoteDemo.init();
});