 google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);



          function makeDivision(length)
      {
          var div_data="";

                 for(var i=0;i<length;i++)
          {
                  
          
           div_data += "<tr ><td><div id='curve_div_" +i+ "'" + "  style='border: 1px solid #ccc'></div></td><td><div id='barchart_div_" +i+ "'" + " style='border: 1px solid #ccc'></div></td></tr>";
          
          } 
          document.getElementById("mult_div").innerHTML = div_data;


      }


      function drawChart(abc) {



             var xhttp = new XMLHttpRequest();


            xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                  var data = this.responseText;
                  var parsed_data = JSON.parse(data);
                  drawTrend(parsed_data);                  

           }
        };
        xhttp.open("POST", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/Ahmedabad/wardanalysis/deptmonthtrend");
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify({ "ward_id": abc}));
     }


       function drawTrend(parsed_data)
       {

           //steps to generate the required data and fill it to array in arrayToDataTable

          var len =  parsed_data[0].length;

          makeDivision(len);
      
          var attribute =[];
           attribute[0]='Month';
           attribute[1]='Total Complains';
           attribute[2]='Solved Complains';
           attribute[3]='Unsolved Complains';
           attribute[4]='Pending Complains';
           attribute[5]='Allocated Complains';
           attribute[6]='user satisfaction from solved complains';
           attribute[7]='user dissatisfaction from solved complains'
                       
          for(var i=0;i<len;i++)
          {
         
            
                       var arrs_month = []; 
                       arrs_month.push(attribute);


//               document.write(JSON.stringify(parsed_data));

                     var dept_name;
                     var check=0;

                  for(var j=0;j<parsed_data.length;j++)
                  {

                  	      dept_name= parsed_data[j][i].department_name;
                  	   if(parsed_data[j][i].total_complains != 0)
                  	    {

            
                                 var check=1;

                        var inst_array = [(j+1) + "/2019",parsed_data[j][i].total_complains,
                        					parsed_data[j][i].solved,
                        					parsed_data[j][i].unsolved,
                        					parsed_data[j][i].pending,
                        					parsed_data[j][i].allocated_complains,
                        					parsed_data[j][i].user_satisfaction_count,
                        					parsed_data[j][i].user_unsatisfaction_count
                        					];
                        	arrs_month.push(inst_array);
                        }
                    }				
             
            //document.write(arrs_month + "<br>");
            if(check==1)
            {

            	 var data = google.visualization.arrayToDataTable(arrs_month);
             
            var options = {
          title: 'Department Complain Trend for ' + dept_name,
          curveType: 'function',
          legend: { position: 'right' },
           animation:{
        duration: 2000,
        easing: 'out',
        "startup": true,
         },
         width:600,
         height :500

        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_div_'+i));

        chart.draw(data, options);


          //draw combo chart
          


          var combo_options = {
          title : 'monthly trend of'+' '+dept_name ,
          vAxis: {title: 'count'},
          hAxis: {title: 'Month'},
          seriesType: 'bars',
          series: {7: {type: 'line'}},
               animation:{
        duration: 2000,
        easing: 'out',
        "startup": true,
         },
         width:600,
         height :500
        };

        var chart = new google.visualization.ComboChart(document.getElementById('barchart_div_'+i));
        chart.draw(data, combo_options); 
        arrs_month=[];
    }
    }

       }

