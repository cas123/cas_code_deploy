
      // Load Charts and the corechart and barchart packages.
      google.charts.load('current', {'packages':['corechart']});

      // Draw the pie chart and bar chart when Charts is loaded.
      google.charts.setOnLoadCallback(drawChart);
      function makeDivision(employee_length)
      {
          var div_data="";

                 for(var i=0;i<employee_length;i++)
          {
                  
          
           div_data += "<tr><td><div id='piechart_div_" +i+ "'" + "  style='border: 1px solid #ccc'></div></td><td><div id='barchart_div_" +i+ "'" + " style='border: 1px solid #ccc'></div></td></tr>";
          
          } 
          document.getElementById("employee_div").innerHTML = div_data;


      }

      function drawChart() {
        
        
  
        //get data here from ajax request
        

           var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
            var parseddata = this.responseText;
            //alert(parseddata);
            getEmployeeAnalysis(JSON.parse(parseddata));
     }
  };
  xmlhttp.open("POST", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/department/analytics");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.send(JSON.stringify({ "department_id": abc}));
    

     }

     function getEmployeeAnalysis(parseddata)
     {
          var emp_ids  = [];

          var emp_map ={};

           

          var employee_length = parseddata.length;





          for(var i=0;i<employee_length;i++)
          {


                    var emp_id = parseddata[i].employee_id;
                     var check=0;
                    for(var j=0;j<emp_ids.length;j++)
                    {


                         if(emp_ids[j]==emp_id)
                         {

                            check=1;
                         }


                    }
                    if(check==0 || emp_ids.length == 0)
                    {


                           emp_ids.push(emp_id);
                           var emp_name = parseddata[i].name;
                           emp_map[emp_id] = emp_name;
               




                    }
                    

          }
              
          var emp_number = emp_ids.length;

          makeDivision(emp_number);

          // for(var i=0;i<employee_length;i++)
          // {

          
          // document.getElementById("employee_div").innerHTML="<tr><td><div id='piechart_div_{$i}'  style='border: 1px solid #ccc'></div></td><td><div id='barchart_div_{$i}'  style='border: 1px solid #ccc'></div></td></tr>";
          
          // }


        

            for(var i=0;i<emp_number;i++)
           {







              //get the count of solved/unsolved/pending/allocated complain for ever employee 


     var pending=0;
     var allocated_complains=0;
     var solved=0;
     var unsolved=0;
     var satisfied =0;
     var unsatisfied=0;
   //  alert(parseddata.length);
     for(var j=0;j<parseddata.length;j++)
     {

       if(parseddata[j].employee_id == emp_ids[i])
       {

         //alert(parseddata[i].status + " " + parseddata[j].employee_id);
        if(parseddata[j].status == "pending" )
        {
            pending = pending +1;
        }
        if(parseddata[j].status == "complain allocated")
        {
          allocated_complains = allocated_complains +1;
        }
        if(parseddata[j].status == "solved")
        {
          solved =solved+1;

          //get the user satisfaction count from it

          if(parseddata[j].user_satisfaction == 1)
          {


              satisfied = satisfied + 1;
          }
          else
          {


              unsatisfied = unsatisfied + 1;
          }
       

        }
        if(parseddata[j].status == "unsolved" )
        {

              unsolved =unsolved+1;


        }
      }

     }
 
        

       //  alert('barchart_div_'+i);

       //create two dimensional array

//       var add_data = [][];


       
        //pie chart data

    var chart_data = google.visualization.arrayToDataTable([['status','count'],['pending',pending],['allocated complains',allocated_complains],['solved',solved],['unsolved',unsolved]]);





    
        // bar graph data

       var bar_data = google.visualization.arrayToDataTable([
         ['user_feedback', 'count', { role: 'style' }],
         ['satisfied', satisfied, 'green'],            // RGB value
         ['unsatisfied', unsatisfied, 'red'],            // English color name
            ]);
        var auth_name = emp_map[emp_ids[i]];
        var auth_id  =  emp_ids[i];

        var piechart_options = {

           title: 'complain status chart for '+ auth_name + ' with id=' + auth_id,
           is3D: true,
           width:500,
           height:300,
           animation:{
        duration: 1000,
        easing: 'out',
        "startup": true,
         }
      

        };
        var piechart = new google.visualization.PieChart(document.getElementById('piechart_div_'+i));
        piechart.draw(chart_data, piechart_options);

        var barchart_options = {title:'User Satisfaction out of total solved complains by'+' '+auth_name,
                       width:500,
                       height:300,
                       legend: 'none',
                          animation:{
                      duration: 2000,
                      easing: 'out',
                     "startup": true,
                      }
        


                     };
        var barchart = new google.visualization.BarChart(document.getElementById('barchart_div_'+i));
        barchart.draw(bar_data, barchart_options);
      }
     



     }
     $(function(){
        
     });