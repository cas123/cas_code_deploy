google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

                  // alert(abc);
                //  alert(abc);
                
             var xhttp = new XMLHttpRequest();


            xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                  var data = this.responseText;
                  //alert(data);
                  var parsed_data = JSON.parse(data);
                  drawTrend(parsed_data);                  

           }
        };
        xhttp.open("POST", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/department/analytics");
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify({ "department_id": abc}));
     }


       function drawTrend(parsed_data)
       {

           //steps to generate the required data and fill it to array in arrayToDataTable

           var attribute =[];
           attribute[0]='Month';
           attribute[1]='Total Complains';
           attribute[2]='Solved Complains';
           attribute[3]='Unsolved Complains';
           attribute[4]='Pending Complains';
           attribute[5]='Allocated Complains';

          //meta data to be included is 

          //1) first find the current year   //we can get this from user selecting the year
          //2) find the minimum and maximum month from where data exist
                //minimum month will be the first record of retirieve data
                //maximum month will be the last record of retrieve data

            //consider we got current year selected as 2019
             
            var curr_year = "2019";    

            var min_month_data = parsed_data[0].created_at;  
            var min_month      = min_month_data.substr(5,2);
            
            //getting the max_month in data
          
            var max_month_data = parsed_data[(parsed_data.length-1)].created_at;  
          
            var max_month      = max_month_data.substr(5,2);


            //generate the all the array required from min month to max month

            var arrs_month = []; 
            arrs_month.push(attribute);

            // var diff = max_month - min_month;
            // var inc=parseInt(min_month);
            // var push_data=[];

            // for(var i=0;i<=diff;i++)
            // {
             
            //     if(i==0)
            //     {
            //        push_data.push((String)(curr_year) + "-" +(String)(min_month));
                  
            //           inc=inc+1;
            //     }else
            //     {
            //      push_data.push((String)(curr_year) + "-0" +(String)(inc));
                 
            //      inc=inc+1;
            //     }
            // }
           

           //count the number of solved/unsolved/pending/complain allocated in from parsed_data for corresponsing month
           //return an array with first index as respective month

           for(var i=parseInt(min_month);i<=max_month;i++)
           {


                var getmonthdata = getMonthData(parsed_data,i,curr_year);

                arrs_month.push(getmonthdata);

  

           } 

//               document.write(JSON.stringify(parsed_data));





            
            

          var data = google.visualization.arrayToDataTable(arrs_month);

        var options = {
          title: 'Department Complain Trend',
          curveType: 'function',
          legend: { position: 'right' },
           animation:{
        duration: 1000,
        easing: 'out',
        "startup": true,
         }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);

       }


       function getMonthData(parsed_data,month_index,curr_year)
       {
             

               var year_month =  curr_year + "-0" + (String) (month_index) ; 

              var instance_array =[];


                   var pending=0;
                   var allocated_complains=0;
                   var solved=0;
                   var unsolved=0;
                   var total_complains = 0;
       for(var i=0;i<parsed_data.length;i++)
      {

        var check_year_month = (parsed_data[i].created_at).substr(0,7);
        
         if(year_month == check_year_month)
         {
          total_complains =total_complains + 1;
        if(parsed_data[i].status == "pending")
        {
            pending = pending +1;
        }
        if(parsed_data[i].status == "complain allocated")
        {
          allocated_complains = allocated_complains +1;
        }
        if(parsed_data[i].status == "solved")
        {
          solved =solved+1;
        }
        if(parsed_data[i].status == "unsolved")
        {

          unsolved =unsolved+1;
        }
      }

     }  
       instance_array.push(year_month);
       instance_array.push(total_complains);
       instance_array.push(solved);
       instance_array.push(unsolved);
       instance_array.push(pending);
       instance_array.push(allocated_complains);
       
       return instance_array;




       }
