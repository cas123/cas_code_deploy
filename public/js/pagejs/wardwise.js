 google.charts.load('current', {'packages':['corechart']});
     // google.charts.setOnLoadCallback(drawVisualization);
     //Draw the pie chart and bar chart when Charts is loaded.
      google.charts.setOnLoadCallback(drawChart);





            function makeDivision(length)
      {
          var div_data="";

                 for(var i=0;i<length;i++)
          {
                  
          
           div_data += "<tr><td><div id='piechart_div_" +i+ "'" + "  style='border: 1px solid #ccc'></div></td><td><div id='barchart_div_" +i+ "'" + " style='border: 1px solid #ccc'></div></td></tr>";
          
          } 
          document.getElementById("mult_div").innerHTML = div_data;


      }


   function drawChart() {

            var xhttp = new XMLHttpRequest();


            xhttp.onreadystatechange = function() {
                   
            if (this.readyState == 4 && this.status == 200) {
                  var data = this.responseText;
                 

                      
                  var parsed_data = JSON.parse(data);

                 len = parsed_data.length;
                 makeDivision(len);

                 for(var i=0;i<len;i++)
                 {

                  var total_complains=parsed_data[i].total_complains;
                  var allocated_complains=parsed_data[i].allocated_complains;
                  var pending=parsed_data[i].pending;
                  var solved=parsed_data[i].solved;

                  var unsolved=parsed_data[i].unsolved;
                  var satisfied=parsed_data[i].user_satisfaction_count;
                  var unsatisfied=parsed_data[i].user_unsatisfaction_count;

                  var gdata=[['status','count',{ role: 'style' }],['total complains',total_complains,'blue'],['allocated complains',allocated_complains,'yellow'],['pending',pending,'orange'],['solved',solved,'purple'],['unsolved',unsolved,'brown'],
            ['user satisfaction from solved complains', satisfied, 'green'],            // RGB value
            ['user dissatisfaction from solved complains', unsatisfied, 'red'],            // English color name
         
            ];

                      
           var chart_data = google.visualization.arrayToDataTable(gdata);

            var bar_data = google.visualization.arrayToDataTable(gdata);
 


                  //draw multiple charts


        var ward_name = parsed_data[i].ward_name;
      //  var auth_id  =  emp_ids[i];

        var piechart_options = {

           title: 'ward complain status chart for '+ ward_name +'with ward_id='+parsed_data[i].ward_id,
           is3D: true,
           width:400,
           height:300

        };
        var piechart = new google.visualization.PieChart(document.getElementById('piechart_div_'+i));
        piechart.draw(chart_data, piechart_options);

        var barchart_options = {title:'ward barchart for complains status of'+' '+ward_name+'with ward_id='+parsed_data[i].ward_id,
                       width:400,
                       height:300,
                       legend:'none',
                          animation:{
                duration: 1000,
                easing: 'out',
              "startup": true,
         }
                       };
        var barchart = new google.visualization.BarChart(document.getElementById('barchart_div_'+i));
        barchart.draw(bar_data, barchart_options);
              

                
        }             
                     
       } 
     };
      
       xhttp.open("get", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/admin/analysis/wardwise");
       // xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send();

       
       }



