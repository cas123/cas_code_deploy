//== Class definition

var DatatableJsonRemoteDemo = function () {
  //== Private functions

  // basic demo
  var demo = function () {

    var datatable = $('.m_datatable').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read:{
            url: 'http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/authority/getcomplains?employee_id='+7,
          },

        },
        pageSize: 10,
        method: 'GET',
      },

      // layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch')
      },

      // columns definition
      columns: [ {
        field: "complain_id",
        title: "ID"
      }, {
        field: "department_name",
        title: "Department",
        
      }, 
      {
        field: "user_first_name",
        title: "User Name",
        width: 70
      }, {
        field: "user_email",
        title: "User Email",
        responsive: {visible: 'lg'}
      }, {
        field: "user_contact",
        title: "User Contact",
        responsive: {visible: 'lg'}
      }, {
        field: "latitude",
        title: "Latitude",
        
      }, 
      {
        field: "logitude",
        title: "Longitude",
        
      }, 
        {
        field: "Actions",
        width: 110,
        title: "Actions",
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
          return '\
            <div class="dropdown ' + dropup + '">\
              <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                <div class="dropdown-menu dropdown-menu-right">\
                  <a class="dropdown-item" href="viewcomplaint"><i class="la la-edit"></i> View Complaint </a>\
                  <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                  <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                </div>\
            </div>\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">\
              <i class="la la-download"></i>\
            </a>\
            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Edit settings">\
              <i class="la la-cog"></i>\
            </a>\
          ';
        }
      }]
    });

    var query = datatable.getDataSourceQuery();

    $('#m_form_status').on('change', function () {
      datatable.search($(this).val(), 'Status');
    }).val(typeof query.Status !== 'undefined' ? query.Status : '');

    $('#m_form_type').on('change', function () {
      datatable.search($(this).val(), 'Type');
    }).val(typeof query.Type !== 'undefined' ? query.Type : '');

    $('#m_form_status, #m_form_type').selectpicker();

  };

  return {
    // public functions
    init: function () {
      demo();
    }
  };
}();

jQuery(document).ready(function () {
  DatatableJsonRemoteDemo.init();
});