//function on button submit
function analyze(abc){
	var year=document.getElementById("year").value;
	var month=document.getElementById("month").value;
	chartdemo(year,month,abc);
	drawChart(year,month,abc);

}




// load and display chart
function chartdemo(year,month,abc){
  //alert("year is"+year+"month"+month+"department"+abc);
google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);
       google.charts.setOnLoadCallback(drawChart);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)

       
       var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {

      if (this.readyState == 4 && this.status == 200) {
            var parseddata = this.responseText;

            compareAreaData(JSON.parse(parseddata));
     }
  };
  xmlhttp.open("POST", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/department/analytics");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.send(JSON.stringify({ "department_id": abc,"year":year,"month":month}));


     
        
      }
     
  function drawChart()
  {

        var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
            var parseddata = this.responseText;
            getAverageStatus(JSON.parse(parseddata));
     }
  };
  xmlhttp.open("POST", "http://cas.mindhackers.org/cas_code_deploy/public/api/municipal/department/analytics");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.send(JSON.stringify({"department_id": abc,"year":year,"month":month}));
    
   
  }






      function compareAreaData(parsedata)
      {

           

     //javascript analysis to put the data in required format

     // want total number of complains in particular area

     //first of all get the array of area having complains
     
      
     var area_array = [];
     area_array.push("areas");
     var check_array_area =[];
     var count_complain_area = [];
     var count_data=[];


     var date_month = parsedata[0].created_at;

     var month =  date_month.substr(0,7);

     count_data.push(month);
       
     for(var i=0;i<parsedata.length;i++)
     {


            var area_id = parsedata[i].area_id;

            //loop through checkarray to check if id exist or not
             var check=0;
             for(var j=0;j<check_array_area.length;j++)
             {
                  if(check_array_area[j] == area_id)
                  {
                    check=1;
                  }


             }
             if(check==0)
             {

                    area_array.push(parsedata[i].area_name);
                    check_array_area.push(parsedata[i].area_id);


             }



     }

     //code to count the number of complains in particular area

   area_array.push("Average");

     for(var i=1;i<(area_array.length-1);i++)
     {
            var count=0;
          for(var j=0;j<parsedata.length;j++)
          {


               if(parsedata[j].area_name == area_array[i] )
               {
                   count=count+1;


               }
          }

        
          count_data.push(count);


     }

     //get the average complain number
     var sum=0;
    
     for(var i=1;i<count_data.length;i++)
     {

        
        sum=sum + parseInt(count_data[i]);

     }

     var average = Math.ceil(sum/(count_data.length-1));
     
     count_data.push(Math.ceil(average));
     

         var data = google.visualization.arrayToDataTable([
          area_array,
          count_data
           ]);

 

        var options = {
          title : 'Monthly complain analysis in your department',
          vAxis: {title: 'total complains'},
          hAxis: {title: 'areas'},
          seriesType: 'bars',
          series: {5: {type: 'line'}},
            animation:{
        duration: 1000,
        easing: 'out',
        "startup": true,
         }
        };

        

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);

        
      }
      function getAverageStatus(parseddata)
      {
           
           var pending=0;
     var allocated_complains=0;
     var solved=0;
     var unsolved=0;
   //  alert(parseddata.length);
     for(var i=0;i<parseddata.length;i++)
     {


        if(parseddata[i].status == "pending")
        {
            pending = pending +1;
        }
        if(parseddata[i].status == "complain allocated")
        {
          allocated_complains = allocated_complains +1;
        }
        if(parseddata[i].status == "solved")
        {
          solved =solved+1;
        }
        if(parseddata[i].status == "unsolved")
        {

          unsolved =unsolved+1;
        }

     }
     
       var chart_data = google.visualization.arrayToDataTable([['status','count'],['pending',pending],['allocated complains',allocated_complains],['solved',solved],['unsolved',unsolved]]);





      var optionsChart = {

           title: 'Average complain status in your department',
           is3D: true,
             animation:{
        duration: 1000,
        easing: 'out',
        "startup": true,
         }
        }
        var pieChart =new google.visualization.PieChart(document.getElementById('piechart_3d'));
        pieChart.draw(chart_data,optionsChart);




      }
}
