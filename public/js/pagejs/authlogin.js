//name check
$.validator.addMethod('check',function(value,element){
	return this.optional(element)||/^[a-z A-Z]+$/.test(value)
},'cannot contain numbers');

//validate form
$.fn.validation=function(){
	//begin validation
	var form=$('#loginform');
	form.validate({
		//rules
		rules:{
			email:{
				required: true,
				email: true
				
			},
			password:{
				required: true,
			}
		},
		messages:{
			email_id:{
				required: "please enter valid email"
			},
			password:{
				required: "please enter password"
			}
		},
		

	});
}
$(function(){
	$.fn.validation();
});