<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema :: connection('ahmedabad_connection')->create('employee', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->string('password');
            $table->string('name');
            $table->string('email_id');
            $table->string('address')->nullable()->default(null);
            $table->integer('ward_id');
            $table->integer('contact_no')->nullable()->default(null);
            $table->integer('department_id');
            $table->string('designation')->nullable()->default(null);
            $table->integer('is_wardAdmin');
            $table->integer('is_departmentAdmin');
            $table->integer('is_AddedAuthority');
            $table->integer('notification_flag');
            $table->integer('notification_count');
            $table->timestamps();
            $table->softDeletes();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
