<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplainSolved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::connection('ahmedabad_connection')->create('complain_solved', function (Blueprint $table) {
            $table->increments('id');
            $table->string('complain_id');
            $table->integer('user_satisfaction');
            $table->timestamps();
            $table->softDeletes();


            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
