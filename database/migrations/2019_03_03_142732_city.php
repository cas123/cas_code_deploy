<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class City extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema :: connection('ahmedabad_connection')->create('city', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->string('name');
          
            $table->integer('state_id');
            $table->integer('country_id');
          
            
           });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
