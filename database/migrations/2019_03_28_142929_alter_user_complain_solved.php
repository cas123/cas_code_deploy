<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserComplainSolved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        \DB::connection('ahmedabad_connection')->statement("ALTER TABLE complain_solved
  ADD proof_doc VARCHAR(200) NULL");
        \DB::connection('ahmedabad_connection')->statement("ALTER TABLE users
  ADD instance_id VARCHAR(200) NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
