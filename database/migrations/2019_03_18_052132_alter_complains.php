<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComplains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \DB::connection('ahmedabad_connection')->statement("ALTER TABLE complains
ADD COLUMN longitude decimal(10,4) NOT NULL AFTER department_id ");
        \DB::connection('ahmedabad_connection')->statement("ALTER TABLE complains
ADD COLUMN latitude decimal(10,4) NOT NULL AFTER department_id ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
