<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplainSubmittedTo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

            Schema :: connection('ahmedabad_connection')->create('complain_submitted_to', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->integer('complain_id');
            $table->string('employee_id');
            $table->string('from_user_id');
             $table->timestamps();
            $table->softDeletes();

           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
