<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegisteredMunicipals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::create('registered_municipals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('municipal_id');
            $table->integer('employee_id');
            $table->string('activation_token');
            $table->string('database_name');
            $table->string('phone');
            $table->tinyInteger('is_verified')->default(0);
            $table->tinyInteger('is_admin');
            $table->integer('last_online')->nullable()->default(null);
            $table->integer('used_storage')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
