<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema :: connection('ahmedabad_connection')->create('user_notification', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->integer('from_employee_id');
            $table->integer('to_user_id');
            $table->string('content');
            $table->string('type');
            $table->integer('is_read');
            $table->timestamps();
            $table->softDeletes();

           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
