<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuperEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('super_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email_id');
            $table->string('password');
            $table->string('prefer_name');
            $table->string('address');
            $table->dateTime('birth_date');
            $table->string('work_phone');
            $table->string('job_title');
            $table->dateTime('email_verified_at')->nullable()->default(null);



            });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
