<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Users;

class Createusertable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

                 // this line is important
     // Illuminate\Support\Facades\DB::setDefaultConnection('ahmedabad_connection');


         Schema :: connection('ahmedabad_connection')->create('users', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->string('phone')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->string('birth_date')->nullable()->default(null);
            $table->integer('notification_flag')->nullable()->default(null);
            $table->string('profile_image')->nullable()->default(null);
            $table->string('device_type')->nullable()->default(null);
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->string('country')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->dateTime('first_login')->nullable()->default(null);
            $table->dateTime('last_active')->nullable()->default(null);
            $table->dateTime('last_login')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->integer('notification_count')->default(0);
            $table->timestamps();
            $table->softDeletes();










     });

  // this line is important, Probably you need to set this to 'mysql'
   //Illuminate\Support\Facades\DB::setDefaultConnection('cas_main_connection');
} 






    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
