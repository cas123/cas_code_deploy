<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Complains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
           Schema :: connection('ahmedabad_connection')->create('complains', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->integer('user_id');
            $table->string('description');
            $table->string('photo');
            $table->string('video')->nullable()->default(null);
            $table->string('status');
            $table->string('type');
            $table->integer('area_id');
            $table->integer('department_id'); 
            $table->timestamps();
            $table->softDeletes();

           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
