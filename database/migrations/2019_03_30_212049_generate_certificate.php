<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateCertificate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('ahmedabad_connection')->create('generated_certificate', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('user_id');
            $table->integer('complain_id');
            $table->string('certificate_link');
             $table->timestamps();
            $table->softDeletes();


        });
  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
