<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WardAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema :: connection('ahmedabad_connection')->create('ward_admin', function (Blueprint $table) {  
      
            $table->increments('id');
            $table->integer('ward_id');
            $table->string('name');
            $table->string('email_id');
            $table->integer('mail_sent');
            
           });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
