<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeSessionData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::connection('ahmedabad_connection')->create('employee_session_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('session_id');
            $table->timestamps();
            $table->softDeletes();


            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
