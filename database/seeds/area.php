<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class area extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        // $departments = array('health','sanitary','crime','culture');
        for($i=1;$i<=30;$i++)
        {
        	for($j=0;$j<15;$j++)
        	{
        		 DB::connection('ahmedabad_connection')->table('area')->insert([
           
                    'name' => Str :: random(5),
                    'city_id' => 1,
                    'state_id' => 1,
                    'country_id' => 1,
                    'ward_id' => $i
              ]);

        	}


        }

    }
}
