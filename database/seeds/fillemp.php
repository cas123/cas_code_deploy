<?php


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Employee;

class fillemp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $result = DB::connection('ahmedabad_connection')->table('department')->select('department.id as department','department.ward_id as ward_id')->get()->toArray();
       

        
          for($i=0;$i<120;$i++)
           {
          

             for($j=0;$j<6;$j++)
             {

              \App\Employee :: create([
           
                  'name' => Str::random(8),
                  'password' => Str :: random(10),
                  'email_id' => Str :: random(6)."@gmail.com",
                  'ward_id' => $result[$i]->ward_id,
                  'department_id' => $result[$i]->department,
                  'is_wardAdmin' => 0,
                  'is_departmentAdmin' => 0,
                  'is_AddedAuthority' => 1,
                  'notification_flag' => 0,
                  'notification_count' => 0,
                  'address' =>  Str :: random(10) . " " .Str::random(8) . " " .  Str :: random(10),
                  'designation' => Str :: random(10),
                  'contact_no' => mt_rand(1000000000,1111111111)
              ]);
            
            }
         }

    }
}
