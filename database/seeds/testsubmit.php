<?php

use Illuminate\Database\Seeder;

class testsubmit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $flag = array(7,7,8,8);
        $j=0;

         for($i=1;$i<30;$i++)
           {
           	if($j==4)
           	{
           		$j=0;
           	}

             DB::connection('ahmedabad_connection')->table('complain_submitted_to')->insert([
           
            'complain_id' => $i,
            'employee_id' => $flag[$j],
            'from_user_id' => $flag[$j++]
              
              ]);
             $j++;
         }
    }
}
