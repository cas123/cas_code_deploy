<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class department extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departments = array('health','sanitary','crime','culture');
        for($i=1;$i<=30;$i++)
        {
        	for($j=0;$j<4;$j++)
        	{
        		 DB::connection('ahmedabad_connection')->table('department')->insert([
           
                    'name' => $departments[$j],
                    'ward_id' => $i
              ]);

        	}


        }



    }
}
