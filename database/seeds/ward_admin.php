<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ward_admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         for($i=1;$i<=30;$i++)
           {
             DB::connection('ahmedabad_connection')->table('ward_admin')->insert([
           
             'name' => Str::random(10),
            'email_id' => Str::random(10).'@gmail.com',
            'ward_id' => $i,
            'mail_sent' => 0
              
              ]);
         }
    }
}
