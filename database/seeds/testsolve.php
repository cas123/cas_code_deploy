<?php

use Illuminate\Database\Seeder;

class testsolve extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $flag = array(0,0,0,1);
        $j=0;

         for($i=0;$i<30;$i++)
           {
           	if($j==4)
           	{
           		$j=0;
           	}

             DB::connection('ahmedabad_connection')->table('complain_solved')->insert([
           
            'complain_id' => $i,
            'user_satisfaction' => $flag[$j]
              
              ]);
             $j++;
         }
    }
}
