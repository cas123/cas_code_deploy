<html>
<head>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
<button onclick="analytics();"> click me</button>
<script>

function analytics()
{



	 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            	
            	  var data = this.responseText;
            	  var parsed_data = JSON.parse(data);

            	  google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);

                 function drawChart() {

                var data = google.visualization.arrayToDataTable([
                ['status', 'count'],
                [parsed_data[0].status, parsed_data[0].count],
                [parsed_data[1].status,  parsed_data[1].count],
                [parsed_data[2].status,  parsed_data[2].count],
                  ]);

        var options = {
          title: 'complain status analytics',
          is3D : true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));

        chart.draw(data, options);
      }
   
            	    


            }
               
        };
        xmlhttp.open("GET", "http://localhost:8000/api/municipal/department/analytics/pie", true);
        xmlhttp.send();
}	
</script>
 <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
</body>
</html>